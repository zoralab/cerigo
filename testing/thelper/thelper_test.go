package thelper

import (
	"fmt"
	"math/big"
	"strings"
	"testing"
	"time"
)

// test values
const (
	labelA                  = "labelA"
	labelB                  = "labelB"
	testScenarioUID         = 41342
	testScenarioTestType    = "exampleTestType"
	testScenarioDescription = "\nA quick brown fox jumps over the lazy dog"
	testScenarioSwitchLabel = "exmpleSwitch"
	testScenarioDataLabel   = "test data label"
	testScenarioDataValue   = "test data value"
)

// testTypes
const (
	testTHelperErrorf                  = "testTHelperErrorf"
	testTHelperLogf                    = "testTHelperLogf"
	testTHelperFlush                   = "testTHelperFlush"
	testTHelperConclude                = "testTHelperConclude"
	testTHelperExpectExists            = "testTHelperExpectExists"
	testTHelperExpectError             = "testTHelperExpectError"
	testTHelperSnapTime                = "testTHelperSnapTime"
	testTHelperCalculateDuration       = "testTHelperCalculateDuration"
	testTHelperCalculateTimeLimits     = "testTHelperCalculateTimeLimits"
	testTHelperExpectInTime            = "testTHelperExpectInTime"
	testTHelperSameBytesSlices         = "testTHelperSameBytesSlices"
	testTHelperSameStrings             = "testTHelperSameStrings"
	testTHelperSameStringSlices        = "testTHelperSameStringSlices"
	testTHelperExpectUIDCorrectness    = "testTHelperExpectUIDCorrectness"
	testTHelperLogScenario             = "testTHelperLogScenario"
	testTHelperExpectSameBigFloat      = "testThelperExpectSameBigFloat"
	testTHelperExpectSameBool          = "testTHelperExpectSameBool"
	testTHelperExpectSameFloat32       = "testThelperExpectSameFloat32"
	testTHelperExpectSameFloat64       = "testThelperExpectSameFloat64"
	testTHelperExpectStringHasKeywords = "testTHelperExpectStringHasKeywords"
)

// switches
const (
	badBoolA                = "badBoolA"
	badBoolB                = "badBoolB"
	badByteA                = "badByteA"
	badByteB                = "badByteB"
	badDuration             = "badDuration"
	badLabelA               = "badLabelA"
	badLabelB               = "badLabelB"
	badMaxTime              = "badMaxTime"
	badMinTime              = "badMinTime"
	badRange                = "badRange"
	badStringA              = "badStringA"
	badStringB              = "badStringB"
	badSubjectA             = "badSubjectA"
	badSubjectB             = "badSubjectB"
	badTimePlacement        = "badTimePlacement"
	beginFromZero           = "beginFromZero"
	emptyByteA              = "emptyByteA"
	emptyByteB              = "emptyByteB"
	emptyFailKeyword        = "emptyFailKeyword"
	emptyScenarioData       = "emptyScenarioData"
	emptyScenarioSwitches   = "emptyScenarioSwitches"
	emptyStringA            = "emptyStringA"
	emptyStringB            = "emptyStringB"
	mismatchedIndex         = "mismatchedIndex"
	mismatchedUID           = "mismatchedUID"
	missingArguments        = "missingArguments"
	missingByteA            = "missingByteA"
	missingByteB            = "missingByteB"
	missingContainer        = "missingContainer"
	missingController       = "missingController"
	missingFailed           = "missingFailed"
	missingLog              = "missingLog"
	missingMaxTime          = "missingMaxTime"
	missingMessage          = "missingMessage"
	missingMinTime          = "missingMinTime"
	missingScenarioData     = "missingScenarioData"
	missingScenarioSwitches = "missingScenarioSwitches"
	missingStartTime        = "missingStartTime"
	missingStopTime         = "missingStopTime"
	missingStringA          = "missingStringA"
	missingStringB          = "missingStringB"
	missingSubjectA         = "missingSubjectA"
	missingSubjectB         = "missingSubjectB"
)

type testController struct {
	logHistory    []string
	failedHistory []string
	failedVerdict string
	logVerdict    string
}

func (c *testController) Logf(format string, args ...interface{}) {
	c.logVerdict = fmt.Sprintf(format, args...)
}

func (c *testController) Errorf(format string, args ...interface{}) {
	c.failedVerdict = fmt.Sprintf(format, args...)
}

type testTHelperScenario struct {
	uid         uint
	testType    string
	description string
	switches    map[string]bool
	inFlushType uint
	inLabel     string
	inSubject   interface{}
	inQuietMode bool
	inExpect    bool
	outValue    int
}

func (s *testTHelperScenario) logf(t *testing.T,
	th *THelper,
	data1 interface{},
	data2 interface{}) {
	var passedVerdict, failedVerdict string

	if th.Controller != nil {
		c := th.Controller.(*testController)
		passedVerdict = c.logVerdict
		failedVerdict = c.failedVerdict
	}

	t.Logf(`
CASE
%v

TEST TYPE
%v

DESCRIPTION
%v


GIVEN:
input switches                  = %v
input quiet mode                = %v
input subject label             = %v
input subject                   = %v
input expectation               = %v

EXPECT:
return value                    = %v

GOT:
THelper has controller          = %v
THelper failed keyword          = %v
THelper quiet mode              = %v
THelper logs container          = %v
THelper failed container        = %v
passed verdict                  = %v
failed verdict                  = %v
return data 1                   = %v
return data 2                   = %v

`,
		s.uid,
		s.testType,
		s.description,
		s.switches,
		s.inQuietMode,
		s.inLabel,
		s.inSubject,
		s.inExpect,
		s.outValue,
		th.Controller != nil,
		th.FailKeyword,
		th.QuietMode,
		th.log,
		th.failed,
		passedVerdict,
		failedVerdict,
		data1,
		data2,
	)
}

func (s *testTHelperScenario) assertUIDCorrectness(t *testing.T, index int) {
	i := uint(index) + 1
	if s.uid != i {
		t.Errorf("CONFIRMFAIL: uid is %v while current index is: %v",
			s.uid,
			i)
	}
}

func (s *testTHelperScenario) assertErrorfMessage(t *testing.T,
	th *THelper, final string) {
	if len(th.failed) == 0 {
		t.Errorf("CONFIRMFAIL: empty error messages container")
		return
	}

	for _, m := range th.failed {
		if !strings.Contains(m, final) {
			t.Errorf("CONFIRMFAIL: missing error message")
			return
		}
	}
}

func (s *testTHelperScenario) assertLogfMessage(t *testing.T,
	th *THelper, final string) {
	if len(th.log) == 0 {
		t.Errorf("CONFIRMFAIL: empty log messages container")
		return
	}

	for _, m := range th.log {
		if m != final {
			t.Errorf("CONFIRMFAIL: missing log message")
			return
		}
	}
}

func (s *testTHelperScenario) assertEmptyContainers(t *testing.T,
	th *THelper) {
	if len(th.log) != 0 {
		switch s.inFlushType {
		case AllType:
			fallthrough
		case LogType:
			t.Errorf("CONFIRMFAIL: log container is not empty")
		default:
		}
	}

	if len(th.failed) != 0 &&
		(s.inFlushType == AllType || s.inFlushType == FailedType) {
		switch s.inFlushType {
		case AllType:
			fallthrough
		case FailedType:
			t.Errorf("CONFIRMFAIL: failed container is not empty")
		default:
		}
	}
}

func (s *testTHelperScenario) assertFilledContainers(t *testing.T,
	th *THelper) {
	if s.switches[missingContainer] {
		return
	}

	if len(th.log) == 0 {
		switch s.inFlushType {
		case AllType:
		case LogType:
		default:
			t.Errorf("CONFIRMFAIL: log container is empty")
		}
	}

	if len(th.failed) == 0 {
		switch s.inFlushType {
		case AllType:
		case FailedType:
		default:
			t.Errorf("CONFIRMFAIL: failed container is empty")
		}
	}
}

func (s *testTHelperScenario) assertConclusionVerdicts(t *testing.T,
	th *THelper) {
	switch {
	case s.switches[missingController] && th.Controller == nil:
		return
	case th.Controller == nil:
		t.Errorf("CONFIRMFAIL: missing controller")
		return
	default:
	}

	c := th.Controller.(*testController)
	ll := len(c.logHistory)
	lf := len(c.failedHistory)

	switch {
	case s.inQuietMode:
		s.assertQuietModeConclusion(t, lf, c)
	case lf != 0:
		s.assertFailedModeConclusion(t, c)
	case ll == 0:
		s.assertNoLogModeConclusion(t, c)
	case ll != 0:
		s.assertLogModeConclusion(t, c)
	}
}

func (s *testTHelperScenario) assertLogModeConclusion(t *testing.T,
	c *testController) {
	if c.logVerdict != "" && c.failedVerdict == "" {
		return
	}

	if c.failedVerdict != "" {
		t.Errorf("CONFIRMFAIL: no failed message in failed verdict")
	}
}

func (s *testTHelperScenario) assertNoLogModeConclusion(t *testing.T,
	c *testController) {
	if c.logVerdict == "" && c.failedVerdict == "" {
		return
	}

	if c.logVerdict != "" {
		t.Errorf("CONFIRMFAIL: has error but got log verdict")
	}

	if c.failedVerdict != "" {
		t.Errorf("CONFIRMFAIL: no failed message in failed verdict")
	}
}

func (s *testTHelperScenario) assertFailedModeConclusion(t *testing.T,
	c *testController) {
	if c.logVerdict == "" && c.failedVerdict != "" {
		return
	}

	if c.logVerdict != "" {
		t.Errorf("CONFIRMFAIL: has error but got log verdict")
	}

	if c.failedVerdict != "" {
		t.Errorf("CONFIRMFAIL: no failed message in failed verdict")
	}
}

func (s *testTHelperScenario) assertQuietModeConclusion(t *testing.T,
	lf int,
	c *testController) {
	if c.logVerdict != "" {
		t.Errorf("CONFIRMFAIL: QuietMode printing passed log")
		return
	}

	if lf != 0 && c.failedVerdict != "" {
		return
	}

	if lf != 0 && c.failedVerdict == "" {
		t.Errorf("CONFIRMFAIL: QuietMode not printing failure")
		return
	}

	if c.failedVerdict != "" {
		t.Errorf("CONFIRMFAIL: no failed message in failed verdict")
	}
}

func (s *testTHelperScenario) assertExistence(t *testing.T,
	th *THelper,
	ret int) {
	if ret != s.outValue {
		t.Errorf("CONFIRMFAIL: ret value is not the same as expected")
		return
	}

	if ret == 0 {
		if len(th.failed) != 0 {
			t.Errorf("CONFIRMFAIL: have error when subject exists")
		}

		return
	}

	if len(th.failed) == 0 {
		t.Errorf("CONFIRMFAIL: empty error when subject exists")
	}
}

func (s *testTHelperScenario) assertTimestamp(t *testing.T, data *time.Time) {
	if data == nil {
		t.Errorf("CONFIRMFAIL: returned timestamp is nil")
	}
}

func (s *testTHelperScenario) assertTimeDuration(t *testing.T,
	data time.Duration,
	duration time.Duration) {
	if data != duration {
		t.Errorf("CONFIRMFAIL: data is not the same as duration")
	}
}

func (s *testTHelperScenario) assertTimeLimits(t *testing.T,
	min *time.Time,
	max *time.Time,
	start *time.Time,
	duration int64,
	ranges int64) {
	if s.switches[missingStartTime] && start == nil {
		return
	}

	if start == nil {
		t.Errorf("CONFIRMFAIL: start time is missing")
		return
	}

	d := time.Duration(duration)
	stop := start.Add(d)

	r := time.Duration(ranges)
	rmin := stop.Sub(*min)
	rmax := max.Sub(stop)

	if rmin != r {
		t.Errorf("CONFIRMFAIL: rmin is not the same as range")
	}

	if rmax != r {
		t.Errorf("CONFIRMFAIL: rmin is not the same as range")
	}

	if d == 0 && stop != *start {
		t.Errorf("CONFIRMFAIL: stop time altered when duration is 0")
	}
}

func (s *testTHelperScenario) assertReturnValue(t *testing.T,
	th *THelper,
	ret int) {
	var expectPassed bool

	if s.outValue == 0 {
		expectPassed = true
	}

	if expectPassed && ret != 0 {
		t.Errorf("CONFIRMFAIL: unexpected return value")
	}

	if ret != 0 && len(th.failed) == 0 {
		t.Errorf("CONFIRMFAIL: missing error message")
	}
}

func (s *testTHelperScenario) assertExpectUIDCorrectness(t *testing.T,
	th *THelper,
	ret int) {
	if ret != s.outValue {
		t.Errorf("CONFIRMFAIL: unexpected return value")
	}

	if ret != 0 && len(th.failed) == 0 {
		t.Errorf("CONFIRMFAIL: missing error message")
	}
}

func (s *testTHelperScenario) prepareMessages() (format string,
	arguments []interface{},
	final string) {
	format = "%v and %v runs on top of hill"
	arguments = []interface{}{
		"Jack",
		"Jill",
	}

	switch {
	case s.switches[missingArguments]:
		arguments = nil
	case s.switches[missingMessage]:
		format = ""
	}

	final = fmt.Sprintf(format, arguments...)

	return format, arguments, final
}

func (s *testTHelperScenario) prepareMissingContainers(th *THelper) {
	if !s.switches[missingContainer] {
		return
	}

	th.log = nil
	th.failed = nil
}

func (s *testTHelperScenario) prepareContainers(th *THelper, m string) {
	if th.Controller == nil {
		return
	}

	c := th.Controller.(*testController)

	if !s.switches[missingLog] {
		th.log = []string{m}
		c.logHistory = th.log
	}

	if !s.switches[missingFailed] {
		th.failed = []string{m}
		c.failedHistory = th.failed
	}
}

func (s *testTHelperScenario) prepareController(th *THelper) {
	th.Controller = &testController{}

	switch {
	case s.switches[missingController]:
		th.Controller = nil
	case s.switches[emptyFailKeyword]:
		th.FailKeyword = ""
	}
}

func (s *testTHelperScenario) prepareQuietMode(th *THelper) {
	if !s.inQuietMode {
		return
	}

	th.QuietMode = true
}

func (s *testTHelperScenario) prepareDurationTime() (start *time.Time,
	stop *time.Time,
	duration time.Duration) {
	t1 := time.Now()
	duration = 5 * time.Millisecond
	t2 := t1.Add(duration)

	start = &t1
	stop = &t2

	if s.switches[badTimePlacement] {
		start = &t2
		stop = &t1
		duration = -duration

		goto exit
	}

	if s.switches[missingStartTime] {
		start = nil
		duration = 0
	}

	if s.switches[missingStopTime] {
		stop = nil
		duration = 0
	}

exit:
	return start, stop, duration
}

func (s *testTHelperScenario) prepareTimeLimits() (start *time.Time,
	duration int64,
	ranges int64) {
	t1 := time.Now()
	d := 5 * time.Millisecond
	r := 3 * time.Millisecond

	start = &t1

	switch {
	case s.switches[missingStartTime]:
		start = nil
	case s.switches[badTimePlacement]:
		start.Add(r)
		start.Add(1 * time.Millisecond)
	}

	if s.switches[badDuration] {
		d = 0
	}

	if s.switches[badRange] {
		r = 0
	}

	return start, int64(d), int64(r)
}

func (s *testTHelperScenario) prepareTimeSet() (subject *time.Time,
	min *time.Time,
	max *time.Time) {
	t1 := time.Now()
	d := 2 * time.Millisecond
	t2 := t1.Add(d)
	t3 := t2.Add(d)

	min = &t1
	subject = &t2
	max = &t3

	switch {
	case s.switches[badMinTime]:
		min = &t2
		subject = &t1
	case s.switches[badMaxTime]:
		max = &t2
		subject = &t3
	}

	if s.switches[missingStopTime] {
		subject = nil
	}

	if s.switches[missingMinTime] {
		min = nil
	}

	if s.switches[missingMaxTime] {
		max = nil
	}

	return subject, min, max
}

func (s *testTHelperScenario) prepareBytesSlices() (aLabel string,
	byteA *[]byte,
	bLabel string,
	byteB *[]byte) {
	aLabel = labelA
	bLabel = labelB
	byteA = &[]byte{0xDE, 0xAD, 0xBE, 0xEF}
	byteB = &[]byte{0xDE, 0xAD, 0xBE, 0xEF}

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badByteA] {
		byteA = &[]byte{0xDE, 0x00, 0xBE, 0x00}
	}

	if s.switches[badByteB] {
		byteB = &[]byte{0xDE, 0xF0, 0xBE}
	}

	if s.switches[emptyByteA] {
		byteA = &[]byte{}
	}

	if s.switches[emptyByteB] {
		byteB = &[]byte{}
	}

	if s.switches[missingByteA] {
		byteA = nil
	}

	if s.switches[missingByteB] {
		byteB = nil
	}

	return aLabel, byteA, bLabel, byteB
}

func (s *testTHelperScenario) prepareUIDSample() (index int,
	uid int,
	startZero bool) {
	index = 0
	uid = 1
	startZero = false

	if s.switches[beginFromZero] {
		startZero = true
		index = 0
		uid = 0
	}

	if s.switches[mismatchedIndex] {
		index = 4
	}

	if s.switches[mismatchedUID] {
		uid = 2
	}

	return index, uid, startZero
}

func (s *testTHelperScenario) prepareStringSlices() (aLabel string,
	sA *[]string,
	bLabel string,
	sB *[]string) {
	aLabel = labelA
	bLabel = labelB
	sA = &[]string{"A",
		"quick",
		"brown",
		"fox",
		"jumps",
		"over",
		"the",
		"lazy",
		"狗"}
	sB = &[]string{"A",
		"quick",
		"brown",
		"fox",
		"jumps",
		"over",
		"the",
		"lazy",
		"狗"}

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badStringA] {
		sA = &[]string{"A",
			"quick",
			"brown",
			"fox",
			"jumps",
			"over",
			"the",
			"lazy",
			"dog"}
	}

	if s.switches[badStringB] {
		sB = &[]string{"the", "dragon", "awakes"}
	}

	if s.switches[emptyStringA] {
		sA = &[]string{}
	}

	if s.switches[emptyStringB] {
		sB = &[]string{}
	}

	if s.switches[missingStringA] {
		sA = nil
	}

	if s.switches[missingStringB] {
		sB = nil
	}

	return aLabel, sA, bLabel, sB
}

func (s *testTHelperScenario) prepareStrings() (aLabel string,
	sA string,
	bLabel string,
	sB string) {
	aLabel = labelA
	bLabel = labelB
	sA = "Unicode你好！"
	sB = "Unicode你好！"

	if s.testType == testTHelperExpectStringHasKeywords {
		sB = "你好"
	}

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badStringA] {
		sA = "ba-zooo-ka"
	}

	if s.switches[badStringB] {
		sB = "Unicode你好!" // the ! is not unicode
	}

	if s.switches[emptyStringA] {
		sA = ""
	}

	if s.switches[emptyStringB] {
		sB = ""
	}

	return aLabel, sA, bLabel, sB
}

//nolint:gocyclo,gocognit
func (s *testTHelperScenario) assertLogScenario(t *testing.T,
	th *THelper,
	data map[string]interface{}) {
	if len(th.log) == 0 {
		t.Errorf("no scenario log messages")
		return
	}

	// case with uid
	subject := th.log[0]
	if !strings.Contains(subject, "CASE") {
		t.Errorf("missing UID title")
	}

	if !strings.Contains(subject, fmt.Sprintf("%v", testScenarioUID)) {
		t.Errorf("missing UID value")
	}

	// test type with value
	subject = th.log[1]
	if !strings.Contains(subject, "TEST TYPE") {
		t.Errorf("missing Test Type title")
	}

	if !strings.Contains(subject, testScenarioTestType) {
		t.Errorf("missing Test Type value")
	}

	// description with value
	subject = th.log[2]
	if !strings.Contains(subject, "DESCRIPTION") {
		t.Errorf("missing description title")
	}

	if !strings.Contains(subject, testScenarioDescription) {
		t.Errorf("missing description value")
	}

	// switches title
	subject = th.log[3]
	if !strings.Contains(subject, "SWITCHES") {
		t.Errorf("missing switches title")
	}

	// switches value
	subject = th.log[4]
	if s.switches[emptyScenarioSwitches] ||
		s.switches[missingScenarioSwitches] {
		if subject != "\n" {
			t.Errorf("missing spacing newline")
		}
	} else {
		if !strings.Contains(subject, testScenarioSwitchLabel) {
			t.Errorf("missing switch value")
		}
	}

	// data section
	if len(th.log) == 5 || len(th.log) == 6 {
		if len(data) != 0 {
			th.Errorf("data did not process while not empty")
		}

		return
	}

	// data title
	subject = th.log[5]
	if !strings.Contains(subject, "GOT") {
		th.Errorf("missing data title")
	}

	// data value
	subject = th.log[6]
	if !strings.Contains(subject, testScenarioDataValue) {
		th.Errorf("missing data value")
	}
}

func (s *testTHelperScenario) prepareScenario() (scenario Scenario,
	data map[string]interface{}) {
	scenario = Scenario{
		UID:         testScenarioUID,
		TestType:    testScenarioTestType,
		Description: testScenarioDescription,
		Switches: map[string]bool{
			testScenarioSwitchLabel: true,
		},
	}

	if s.switches[emptyScenarioSwitches] {
		scenario.Switches = map[string]bool{}
	}

	if s.switches[missingScenarioSwitches] {
		scenario.Switches = nil
	}

	data = map[string]interface{}{
		testScenarioDataLabel: testScenarioDataValue,
	}

	if s.switches[emptyScenarioData] {
		data = map[string]interface{}{}
	}

	if s.switches[missingScenarioData] {
		data = nil
	}

	return scenario, data
}

func (s *testTHelperScenario) prepareBool() (aLabel string,
	sA bool,
	bLabel string,
	sB bool) {
	aLabel = labelA
	bLabel = labelB
	sA = true
	sB = true

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badBoolA] {
		sA = false
	}

	if s.switches[badBoolB] {
		sB = false
	}

	return aLabel, sA, bLabel, sB
}

func (s *testTHelperScenario) prepareFloat32() (aLabel string,
	sA float32,
	bLabel string,
	sB float32) {
	aLabel = labelA
	bLabel = labelB
	sA = float32(23.2341e12)
	sB = sA

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badSubjectA] {
		sA = 0
	}

	if s.switches[badSubjectB] {
		sB = 0
	}

	return aLabel, sA, bLabel, sB
}

func (s *testTHelperScenario) prepareFloat64() (aLabel string,
	sA float64,
	bLabel string,
	sB float64) {
	aLabel = labelA
	bLabel = labelB
	sA = float64(23.2341e12)
	sB = sA

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badSubjectA] {
		sA = 0
	}

	if s.switches[badSubjectB] {
		sB = 0
	}

	return aLabel, sA, bLabel, sB
}

func (s *testTHelperScenario) prepareBigFloat() (aLabel string,
	sA *big.Float,
	bLabel string,
	sB *big.Float) {
	aLabel = labelA
	bLabel = labelB
	sA = big.NewFloat(float64(23.2341e12))
	sB = big.NewFloat(float64(23.2341e12))

	if s.switches[badLabelA] {
		aLabel = ""
	}

	if s.switches[badLabelB] {
		bLabel = ""
	}

	if s.switches[badSubjectA] {
		sA = big.NewFloat(float64(0))
	}

	if s.switches[badSubjectB] {
		sB = big.NewFloat(float64(0))
	}

	if s.switches[missingSubjectA] {
		sA = nil
	}

	if s.switches[missingSubjectB] {
		sB = nil
	}

	return aLabel, sA, bLabel, sB
}
