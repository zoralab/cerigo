package thelper

import (
	"testing"
)

func TestExpectSameFloat64(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperExpectSameFloat64 {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		labelA, sA, labelB, sB := s.prepareFloat64()

		// test
		ret := th.ExpectSameFloat64(labelA, sA, labelB, sB)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertReturnValue(t, th, ret)
		s.logf(t, th, ret, "N/A")
	}
}
