package thelper

import (
	"fmt"
	"testing"
)

//nolint:wsl
func Example() {
	t := &testing.T{}

	/***************************
	 * Basic Reporting         *
	 ***************************/
	// 1. Create Helper
	h := THelper{
		Controller:  t,
		FailKeyword: "TESTFAIL",
		QuietMode:   false,
	}

	// 2. Log messages
	h.Logf("This is a log message")
	h.Errorf("This is an error message that will fail the test")

	// 3. Conclude the test and print test messages at the end of the test
	h.Conclude()

	/****************************
	 * Assertion                *
	 ****************************/
	// 1. data existence
	a := []byte("Hello World")
	h.ExpectExists("demo bytes", a, true)  // pass
	h.ExpectExists("demo nil", nil, true)  // fail
	h.ExpectExists("demo nil", nil, false) // pass

	// 2. error existence
	err := fmt.Errorf("demo error")
	h.ExpectError(err, true)  // pass
	h.ExpectError(nil, true)  // fail
	h.ExpectError(nil, false) // pass

	// 3. Time sensitive assertion
	start := h.SnapTime()
	fmt.Printf("%v jumps over %v and made it to the %v",
		"James",
		"Lina",
		"Mountain")
	stop := h.SnapTime()
	myDuration := int64(300000) // 300 miliseconds
	minimum, maximum := h.CalculateTimeLimits(start, myDuration, 25000)
	h.ExpectInTime(stop, minimum, maximum)

	// 4. Byte Slices compare
	bSliceA := []byte{0xDE, 0xAD, 0xBE, 0xEF}
	bSliceB := []byte{0xde, 0xad, 0xbe, 0xef}
	h.ExpectSameBytesSlices("target", &bSliceA, "reference", &bSliceB)

	// 5. String Slices compare
	sSliceA := []string{"Hello", "world"}
	sSliceB := []string{"Bonjour", "World"}
	h.ExpectSameStringSlices("target", &sSliceA, "reference", &sSliceB)

	// 6. Same Strings compare
	h.ExpectSameStrings("subject", "Hello", "reference", "Bonjour")

	// 7. String Has Substring
	h.ExpectStringHasKeywords("subject", "Hello World", "keyword", "World")

	// 8. Same Boolean
	h.ExpectSameBool("subject", true, "reference", true)
}
