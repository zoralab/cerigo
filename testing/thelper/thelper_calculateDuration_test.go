package thelper

import (
	"testing"
)

func TestCalculateDuration(t *testing.T) {
	scenarios := testTHelperScenarios()

	for i, s := range scenarios {
		if s.testType != testTHelperCalculateDuration {
			continue
		}

		// prepare
		th := NewTHelper(t)
		s.prepareController(th)
		s.prepareQuietMode(th)
		s.prepareMissingContainers(th)
		start, stop, duration := s.prepareDurationTime()

		// test
		data := th.CalculateDuration(start, stop)

		// assert
		s.assertUIDCorrectness(t, i)
		s.assertTimeDuration(t, data, duration)
		s.logf(t, th, data, duration)
	}
}
