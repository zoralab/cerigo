+++
# WARNING: This markdown is autogenerated by bot. Do not edit it manually!
date = "2020-09-21T11:52:16+08:00"
title = "filehelper"
description = """

"""
keywords = [ "filehelper", "go", "cerigo", "docs" ]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "/img/thumbnails/go/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo Go Documents"

[thumbnails.1]
url = "/img/thumbnails/go/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo Go Documents"


[menu.main]
parent = "X) Go Doc"
name = "os/file/filehelper"
pre = "📘"
weight = 1


[schema]
selectType = "WebPage"
+++

# Package filehelper
```go
import "gitlab.com/zoralab/cerigo/os/file/filehelper"
```





## Constants

```go
const (
	// Byte1K is the 1k ChunkSize
	Byte1K = uint(1000)


	// Byte4K is the 4k ChunkSize
	Byte4K = uint(4000)


	// Byte8K is the 8k ChunkSize
	Byte8K = uint(8000)


	// Byte64K is the 64k ChunkSize
	Byte64K = uint(64000)


	// Byte128K is the 128k ChunkSize
	Byte128K = uint(128000)
)
```





## FileHelper
```go
type FileHelper struct {
	ChunkSize uint
}
```
FileHelper is a structure for hosting helper functions related to files. It
has a public element:
  1. ChunkSize - setting for read/write in packet size. The recommended
                 value is Byte64K, depending on the computational
                 availability. You can set any values at your will. If
                 nothing is set, it uses Byte4K as its default chunk size.



### `Func (f *FileHelper) Compare(path1 string, path2 string) (verdict bool)`
Compare is to perform byte-to-byte comparison for 2 given files. This
function uses low memory deep compare algorithm to perform file comparison.
It compares bytes by bytes between the files until the first byte differences
is found. In that case, it returns the verdict immediately.

All being said, this function by itself is not suitable for cryptography
requirements since the comparison timing may introduce side-channel timing
attack.

Therefore, this function can perform slowly if either of the files are very
large (due to algorithm).

It returns:
  1. true          - both filepaths are having the same contents
  2. false         - both filepaths are not having the same contents



### `Func (f *FileHelper) FileExists(path string) bool`
FileExists is to check whether a file/directory exists



### `Func (f *FileHelper) FileHasKeywords(path string, keywords ...string) (list []int)`
FileHasKeywords is to check whether a keyword exists in the file.
It takes inputs of:
  1. path              - path to file
  2. keywords          - the keywords. It can be 1 or many

It returns:
  1. empty list        - problem opening the file or no results found.
  2. numbers list      - an array of line numbers with matched keyword(s).



### `Func (f *FileHelper) SameStat(path1 string, path2 string) (verdict bool)`
SameStat checks the given two files having the same inode stat. If it
does, it will return true, which means both filepaths are referring to the
same file. Only filepath that fulfilles os.SameFile works here.

It returns:
  1. true  - both filepaths are referring to the same file.
  2. false - the filepaths are not referring to the same file.



### `Func (f *FileHelper) SimilarStat(path1 string, path2 string) (verdict bool)`
SimilarStat checks the given two paths are having similar file pattern such
as its directory/file nature, size and modes. It excludes modification time
and name for further comparison. This function only evaluates the following
aspects:
  1. Same size
  2. Same mode (file's mode and permission bits - os.FileMode)
  3. Same type (directory / file)

SimilarStat will always return false for anything else, including error
situations.
