+++
date = "2020-09-21T13:15:22+08:00"
title = "Releases"
description = """
Each Cerigo releases always come with a release note. Depending on the version
you are using, you can check out the changes listed in the release note and
apply them accordingly. This section lists every release notes made by Cerigo.
"""
keywords = ["releases"]
draft = false
type = ""
# redirectURL=""
layout = "list"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "releases/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo"

[thumbnails.1]
url = "releases/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo"


[menu.main]
parent = "redirect"
name = "Release Notes"
pre = "🗃️"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}
