+++
date = "2020-09-21T11:58:07+08:00"
title = "Upstreaming Process"
description = """
Contributing codes and materials to Cerigo is quite straight forward: abide
to its upstream process. This section covers specifically on the important
steps for upstreaming your codes into Cerigo code repository.
"""
keywords = ["Cerigo", "upstream", "process"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "upstream-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Cerigo Upstreaming Process"

[thumbnails.1]
url = "upstream-1200x628.png"
width = "1200"
height = "628"
alternateText = "Cerigo Upstreaming Process"


[menu.main]
parent = ""
name = "Upstreaming Process"
pre = "📤"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}




## Ground Rules
The only **1 strict** requirement we impose here is your
**`git` commit message**. You **MUST** comply to our message pattern in order
not to break our build system. Here are the critical reasons.



### 1. CHANGELOG.md Automation
We use `git log --oneline` to generate our CHANGELOG.md for every releases
automatically.



### 2. We read Git Log Very Often
From our day-to-day operations, we work with terminal based interface locally
over a straight 12~20+ hours (highest record) per day. We simply do not have
the time to visit a website just to view past commits list.



### 3. Remove Dependency
The git log system was initially designed without these version control cloud
system such as Github or GitLab. Hence, we want to maintain such historical
tracking in our repository rather than storing the data in an external system.

This is a single source repository, let's not introduce unnecessary dependency.




## GnuPG Enhanced Commit
To ensure **you are you** (integrity checking), the commit should be signed
using the GnuPG system. Please ensure your local `git` system is using GnuPG
signing system before proceeding and have the public key available at:

http://keys.gnupg.net/pks/lookup?op=vindex&fingerprint=on&search=0x02CACE317AE8191A

You can send your key to the designated keyserver:
```bash
$ gpg --keyserver hkps://hkps.pool.sks-keyservers.net --send-keys <key-id>
```

You can check your key's availability via:

```bash
$ gpg --keyserver hkps://hkps.pool.sks-keyservers.net --search-keys <email>`
```

or you can perform the following:

```bash
$ gpg --keyserver hkps://hkps.pool.sks-keyservers.net --recv-keys <key-id>
```



### Commit Signature
There should be a `Signed-off-by: Full Name <email>` in every of your commit. A
proper `signed-off` commit with GnuPG message is something as such when viewed
using `git log --show-signatures`:
```
commit 01b2208172fc880fbd203fa19f61a84edb546169
gpg: Signature made 2018-07-28T18:23:40 +08
gpg:                using RSA key <local key ID>
gpg: Good signature from "(Holloway) Chew, Kean Ho (ZORALab developer) <kean.ho.chew@zoralab.com>" [ultimate]
Author: (Holloway) Chew, Kean Ho <kean.ho.chew@zoralab.com>
Date:   Sat Jul 28 18:22:22 2018 +0800

    coding_guidelines/yml.md: updated yml to latest consistency

    The existing guideline is out-of-date and some are complicated.
    yml file should be self-explainatory and consistent for simplicty
    sake.

    This patch updates the guideline to the latest, keeping things
    easier to use.

    Signed-off-by: (Holloway) Chew, Kean Ho <kean.ho.chew@zoralab.com>
```




## Commit Format
There is absolutely not reason for you not to write a good commit message. Here
are the definitions of **good**.



### Column Width
Keep it `75` for short description, `80` for body as required by `git`.



### Keep it Small
Keep your commit as small as **ONLY holding 1 purpose**.

We got enough work on our plate already and don't make us to research your patch
(left alone reviewing your codes). It's very easy to sniff out a big commit:
the use of **AND** word(s) in your short line.

Here is an example of a big commit:

```
name/file - add feature A and feature B into repository
```

Here what should have done (2 commits):

```
name/file - add feature A into repository
name/file - add feature B into repository
```



### Write like a Patch
Do you know that each commits can be a patch file? There commands available for
it:

```bash
# create a patch file by commit ID
$ git format-patch -p1 <commit_id> -o /path/to/output/directory

# importing a patch file into repository
$ git am --signoff /path/to/patches/*.patch
```
This is a reason why you should write your commit message as crisp and clear
without depending on other tool.



### Final Pattern
A good commit message should have the following pattern:
```
<WHERE>/<FILENAME> - WHAT IS THIS PATCH ABOUT MAX 75 WIDTH

Explain WHAT is the problem? How big is the IMPACT?
Include data like system dump here as well. Just
write it as clear and detailed as possible.
It is perfectly fine to have a long commit than asking
me to read things over all the places!

In new paragraph, explain WHY we need to fix it.
Why must we apply this patch. How to we approach the
problem. Detailed your reasoning and an overview
of your efforts (but don't have to explain the codes).

In new paragraph, explain what's is patch does in a
nutshell. Don't explain the codes. We can read them  using
`git diff`. Remember to sign the commit so the best
command would be `git commit -s`.

Signed-off-by: FULLNAME <email@address.ends>
```




## Process
These are the sequences to get you started.



### Step 1 - Always File an Issue
The very first step is file an issue in the
[Issues Section](https://gitlab.com/ZORALab/cerigo/issues). All discussions
should be ready and maintainers are aware and ready for your changes.

{{< note "default" "Goal" >}}
**To avoid shocking surprises and potential heart attacks, fatigues, and
burn out.**
{{< /note >}}



### Step 2 - Setting Up
Once the idea is accepted, you may proceed to fork the repository and work
on it. Normally, we work on `next` branch so it's safe to use the following
command pattern:
```
$ git clone -b <next branch> <URL>
```

Please ensure you setup the necessary continuous integration system following
the project description. If you're using GitLab CI and the repository is
configured to use shared runner, it is done automatically.


{{< note "default" "Goal" >}}
**To synchonize with the local setup**
{{< /note >}}



### Step 3 - Development
You may begin your development. As you develop your codes, we gently remind you
that:

1. Comply to our ground rules.
2. Comply to proper coding style.
3. Keep your commit small (without using `and` in your short-line).
4. Remove secret files.
5. Sign your commit with your GnuPG.



### Step 4 - Test Your Code
Please **TEST** your code. The definitions of **Done** are:

{{< note "default" "Goal" >}}
1. New code developments are completed **AND**
2. Test script is completed, tested **AND**
3. Documentations are updated **AND**
{{< /note >}}



### Step 5 - Remove Secrets
This step is optional if you do your Step 3 properly. However, please do it
as a last check before commiting out to the network.



### Step 6 - Rebase with Remote `next` Branch
Chances are, by the time you completed your project, the remote `next` branch
is having the latest and greatest. Hence, you'll need to synchonize with it
before pushing out to your forked repository. Here are the instructions to do
that:
```bash
$ git remote add upstream <remote original branch>
$ git fetch upstream
$ git rebase upstream/<remote original branch name>
<solve any conflicts>
$ git push -f
```



### Step 7 - Raise Merge Request
You're ready for merge request. Process to one. Please make sure you update
your `issue` in the
[Issues Section](https://gitlab.com/ZORALab/cerigo/issues).
