<!--
+++
title = "Cerigo"
type = ""
layout = "single"
data = "2019-08-06T13:08:02+08:00"
draft = false
authors = ["ZORALab", "Holloway Chew Kean Ho"]
description = """
Cerigo是个Go编程语言的组件。它的来历是延伸目前Go的标准住代码库，能快速的帮助程
序编制。
"""
keywords = ["编程", "组件", "编程组件", "Cerigo", "Go", "ZORALab"]
# thumbnailURL = "#"

[menu.main]
name = "主页"
weight = 1
+++
-->
![Cerigo-Banner](https://lh3.googleusercontent.com/MgWd3wEghG8BIbbZYIrVaHcprIeRSEdLOyY3Bdu2Lyu9yJGl-46xNQQyBoqWCyR-84xyloVL27S1mcIAnOG75ZoXeY1AhTipuAEGlgPhHOC7_yzHQKuEAGPm_F3nVJmI7R4mY5MP3Kd9Id6_AkCqbG0GAgbt8vSPO8r-nMfb6ktw6AoO6oqkqS4ekWyg_PNI3WVw-xp-XripTpktbarvUQHAfj7Jv3CTOPKbg38h4VDToULZvA0-DGJvpumR7-8wDCxmC_kO-_6fpRugr0mb4dFXpPam6mTzepbdplfycGp7jUTRGLBw3SHXEjfAH88AZRuvFW6jVaHdnPhTz05nqw5lauGQyr9QNHt1w9TU9x7iLEPjuFJxs0T6DWgEj79qj_hzlQLpkByqdems_lVmfrJqeyEu0pxO-lS5EXqCu1fR7LXD5N5-00fuGqXUwHgWbNufCWvR-aVIjl05NwXyIZqy2yC1cTlbPX3inQ-7010PXjf9i24Ew8pRVNpSdp2k8SWy8QE6VBa-YhpauyGAvCYC-ZnT7HZNeJ4b9uX7suLEH4zxf0WomaHYztamjjzZZhAgJK-d3T70j_fHRgK5gpSdZCPBXYSu_BtOvP8flS_4hzV2jGRcLVh11H9l205k1QjgeEWhLrrG-36P-Zpd8yyXqPJonV1p-d9nB0U9DXIZnak1QQzc9cHzovkuwF18VmUUOryFfJagKrQRVQ=w1790-h895-no)

# Cerigo
Cerigo是个Go编程语言的组件。它的来历是延伸目前Go的标准代码库，能快速的帮助
程序编制。Cerigo是配合Go标准代码库一起配用的。

Cerigo运用[Go Module](https://blog.golang.org/using-go-modules)来维持它的软件发布。这说明它是可以完全配合Go 1.11 版本以上。如果运用Go 1.11 一下的版本，恐怕
会有无法配合的问题。Cerigo没有向后兼容性的发展计划。

Cerigo是运用英语作为主要沟通语言，是和世界科学评议会(International Council for
Science)对齐。一切误会，请您谅解。如果需要观光一切文件，请去:
**[{{< absLink "/en-us/" >}}]({{< absLink "/en-us/" >}})**

## 知识库状况

![语言-Go](https://img.shields.io/badge/语言-go-1313c3.svg?style=for-the-badge)
![执照-Apache2](https://img.shields.io/badge/执照-APACHE%202%2E0-orange.svg?style=for-the-badge)
![发布素质-Alpha](https://img.shields.io/badge/发布素质-alpha-red.svg?style=for-the-badge)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/zoralab/cerigo)](https://goreportcard.com/report/gitlab.com/zoralab/cerigo)


| 分枝     | 实验状况    | 实验范围率    |
|:---------|:------------|:--------------|
| `master` | ![master pipeline status](https://gitlab.com/ZORALab/cerigo/badges/master/pipeline.svg) | ![master coverage report](https://gitlab.com/ZORALab/cerigo/badges/master/coverage.svg) |
| `staging` | ![staging pipeline status](https://gitlab.com/ZORALab/cerigo/badges/staging/pipeline.svg) | ![staging coverage report](https://gitlab.com/ZORALab/cerigo/badges/staging/coverage.svg) |
| `next`   | ![next pipeline status](https://gitlab.com/ZORALab/cerigo/badges/next/pipeline.svg) | ![next coverage report](https://gitlab.com/ZORALab/cerigo/badges/next/coverage.svg) |


## 赞助商
*这计划是有以下的赞助商帮助：*

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)
