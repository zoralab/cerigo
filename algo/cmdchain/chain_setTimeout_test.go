package cmdchain

import (
	"testing"
)

func TestSetTimeout(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testSetTimeout {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		timeout := s.prepareTimeout(c)

		// test
		c.SetTimeout(timeout)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertTimeout(th, c)
		s.log(th, map[string]interface{}{
			"input timeout": timeout,
			"got timeout":   c.timeout,
		})
		th.Conclude()
	}
}
