package cmdchain

import (
	"testing"
)

func TestInterrupts(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testInterrupts {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		cmd, _, _ := s.prepareChain(c)
		cmd = s.prepareInterrupts(cmd)

		// test
		c.Interrupts(cmd)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertInterrupts(th, c, cmd)
		s.log(th, map[string]interface{}{
			"input cmd": cmd,
			"got":       c.interrupts,
		})
		th.Conclude()
	}
}
