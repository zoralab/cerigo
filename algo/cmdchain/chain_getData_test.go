package cmdchain

import (
	"testing"
)

func TestGetData(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testGetData {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		_ = s.prepareData(c)

		// test
		data := c.GetData()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertData(th, c, data)
		s.log(th, map[string]interface{}{
			"input data": c.data,
			"got":        data,
		})
		th.Conclude()
	}
}
