package cmdchain

const (
	// ErrorTimeout is the error message for chain timeout.
	ErrorTimeout = "timeout waiting for chain completion"

	// ErrorMissingCommand is the error message for missing registered
	// command
	ErrorMissingCommand = "missing command from chain registered list"

	// ErrorExistingCommand is the error message for command already
	// exists before registering a new one with same label.
	ErrorExistingCommand = "command already exists"

	// ErrorBadLabel is the error message for command label that is not
	// allowed to use.
	ErrorBadLabel = "command label cannot be 0"
)
