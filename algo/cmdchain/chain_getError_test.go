package cmdchain

import (
	"testing"
)

func TestGetError(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testGetError {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		expect := s.prepareErrors(c)

		// test
		err := c.GetError()

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertError(th, err, expect)
		s.log(th, map[string]interface{}{
			"input error": c.err,
			"got":         err,
		})
		th.Conclude()
	}
}
