package cmdchain

func testChainScenarios() []testChainScenario {
	return []testChainScenario{
		{
			UID:      1,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 command
`,
			Switches: map[string]bool{},
		}, {
			UID:      2,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 command
2. bad timeout
`,
			Switches: map[string]bool{
				useBadTimeout: true,
			},
		}, {
			UID:      3,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 command
2. zero timeout
`,
			Switches: map[string]bool{
				useZeroTimeout: true,
			},
		}, {
			UID:      4,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 command
2. bad timeout
`,
			Switches: map[string]bool{
				useBadTimeout: true,
			},
		}, {
			UID:      5,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 command
2. bad timeout
3. proper error handling
`,
			Switches: map[string]bool{
				useBadTimeout: true,
				prepareError:  true,
			},
		}, {
			UID:      6,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. proper error handling
`,
			Switches: map[string]bool{
				prepareError:         true,
				useInterfaceFunction: true,
			},
		}, {
			UID:      7,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. use bad timeout
3. proper error handling
`,
			Switches: map[string]bool{
				prepareError:         true,
				useBadTimeout:        true,
				useInterfaceFunction: true,
			},
		}, {
			UID:      8,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. proper error handling
`,
			Switches: map[string]bool{
				prepareIntercept:     true,
				prepareError:         true,
				useInterfaceFunction: true,
			},
		}, {
			UID:      9,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. proper lv3 intercepting interfacing command
3. proper error handling
4. missing intercept command
`,
			Switches: map[string]bool{
				prepareIntercept:     true,
				prepareError:         true,
				useInterfaceFunction: true,
				missingInterceptCMD:  true,
			},
		}, {
			UID:      10,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. proper lv2 interrupting interfacing command
3. proper error handling
4. missing intercept command
`,
			Switches: map[string]bool{
				prepareInterrupt:     true,
				prepareError:         true,
				useInterfaceFunction: true,
			},
		}, {
			UID:      11,
			TestType: testRun,
			Description: `
Run to work properly with given:
1. proper lv4 interfacing command
2. proper lv3 intercepting interfacing command
3. proper lv2 interrupting interfacing command
3. proper error handling
4. missing intercept command
`,
			Switches: map[string]bool{
				prepareInterrupt:     true,
				prepareIntercept:     true,
				prepareError:         true,
				useInterfaceFunction: true,
				missingInterceptCMD:  true,
			},
		}, {
			UID:      12,
			TestType: testCMDExists,
			Description: `
CMDExists should work properly when given:
1. a proper label
2. an existing command
`,
			Switches: map[string]bool{
				prepareIntercept: true,
			},
		}, {
			UID:      13,
			TestType: testCMDExists,
			Description: `
CMDExists should work properly when given:
1. a bad label
2. an existing command
`,
			Switches: map[string]bool{
				prepareIntercept:     true,
				useBadInterceptLabel: true,
			},
		}, {
			UID:      14,
			TestType: testCMDExists,
			Description: `
CMDExists should work properly when given:
1. a proper label
2. a missing command
`,
			Switches: map[string]bool{},
		}, {
			UID:      15,
			TestType: testIntercept,
			Description: `
Intercept should work properly when given:
1. a proper label
2. a proper command
`,
			Switches: map[string]bool{
				prepareIntercept: true,
			},
		}, {
			UID:      16,
			TestType: testIntercept,
			Description: `
Intercept should work properly when given:
1. a bad label
2. a proper command
`,
			Switches: map[string]bool{
				prepareIntercept:     true,
				useBadInterceptLabel: true,
			},
		}, {
			UID:      17,
			TestType: testIntercept,
			Description: `
Intercept should work properly when given:
1. a proper label
2. a missing command
`,
			Switches: map[string]bool{},
		}, {
			UID:      18,
			TestType: testRegister,
			Description: `
Register should work properly when given:
1. a proper label
2. a proper command
3. no pre-registered command
`,
			Switches: map[string]bool{},
		}, {
			UID:      19,
			TestType: testRegister,
			Description: `
Register should work properly when given:
1. a bad label
2. a proper command
3. no pre-registered command
`,
			Switches: map[string]bool{
				useBadInterceptLabel: true,
			},
		}, {
			UID:      20,
			TestType: testRegister,
			Description: `
Register should work properly when given:
1. a proper label
2. a proper command
3. pre-registered command
`,
			Switches: map[string]bool{
				prepareIntercept: true,
			},
		}, {
			UID:      21,
			TestType: testRegister,
			Description: `
Register should work properly when given:
1. a proper label
2. a missing command
3. no pre-registered command
`,
			Switches: map[string]bool{
				missingInterceptCMD: true,
			},
		}, {
			UID:      22,
			TestType: testDelete,
			Description: `
Delete should work properly when given:
1. a proper label
2. a pre-registered command
`,
			Switches: map[string]bool{
				prepareIntercept: true,
			},
		}, {
			UID:      23,
			TestType: testDelete,
			Description: `
Delete should work properly when given:
1. a proper label
2. no pre-registered command
`,
			Switches: map[string]bool{},
		}, {
			UID:      24,
			TestType: testDelete,
			Description: `
Delete should work properly when given:
1. a bad label
2. a pre-registered command
`,
			Switches: map[string]bool{
				prepareIntercept:     true,
				useBadInterceptLabel: true,
			},
		}, {
			UID:      25,
			TestType: testInterrupts,
			Description: `
Interrupts should work properly when given:
1. a proper command
2. no pre-registered interrupts
`,
			Switches: map[string]bool{},
		}, {
			UID:      26,
			TestType: testInterrupts,
			Description: `
Interrupts should work properly when given:
1. a proper command
2. a pre-registered interrupts
`,
			Switches: map[string]bool{
				prepareInterrupt: true,
			},
		}, {
			UID:      27,
			TestType: testInterrupts,
			Description: `
Interrupts should work properly when given:
1. a missing command
2. no pre-registered interrupts
`,
			Switches: map[string]bool{
				missingInterruptCMD: true,
			},
		}, {
			UID:      28,
			TestType: testGetError,
			Description: `
GetError should work properly when given:
1. a proper error
`,
			Switches: map[string]bool{
				prepareError: true,
			},
		}, {
			UID:      29,
			TestType: testGetError,
			Description: `
GetError should work properly when given:
1. a nil error
`,
			Switches: map[string]bool{
				prepareError: true,
				missingError: true,
			},
		}, {
			UID:      30,
			TestType: testSetError,
			Description: `
SetError should work properly when given:
1. a proper error
`,
			Switches: map[string]bool{},
		}, {
			UID:      31,
			TestType: testSetError,
			Description: `
SetError should work properly when given:
1. a missing error
`,
			Switches: map[string]bool{
				prepareError: true,
				missingError: true,
			},
		}, {
			UID:      32,
			TestType: testSetErrorHandler,
			Description: `
SetErrorHandler should work properly when given:
1. a proper command
2. no preset command
`,
			Switches: map[string]bool{},
		}, {
			UID:      33,
			TestType: testSetErrorHandler,
			Description: `
SetErrorHandler should work properly when given:
1. a proper command
2. a preset command
`,
			Switches: map[string]bool{
				prepareError: true,
			},
		}, {
			UID:      34,
			TestType: testSetErrorHandler,
			Description: `
SetErrorHandler should work properly when given:
1. a missing command
2. no preset command
`,
			Switches: map[string]bool{
				missingErrorHandlingCMD: true,
			},
		}, {
			UID:      35,
			TestType: testSetData,
			Description: `
SetData should work properly when given:
1. a proper data
2. no preset data
`,
			Switches: map[string]bool{},
		}, {
			UID:      36,
			TestType: testSetData,
			Description: `
SetData should work properly when given:
1. a missing data
2. no preset data
`,
			Switches: map[string]bool{
				prepareNoData: true,
			},
		}, {
			UID:      37,
			TestType: testSetData,
			Description: `
SetData should work properly when given:
1. a proper data
2. with preset data
`,
			Switches: map[string]bool{
				presetData: true,
			},
		}, {
			UID:      38,
			TestType: testGetData,
			Description: `
GetData should work properly when given:
1. with preset data
`,
			Switches: map[string]bool{
				presetData: true,
			},
		}, {
			UID:      39,
			TestType: testGetData,
			Description: `
GetData should work properly when given:
1. without preset data
`,
			Switches: map[string]bool{},
		}, {
			UID:      40,
			TestType: testSetTimeout,
			Description: `
SetTimeout should work properly when given:
1. good timeout
`,
			Switches: map[string]bool{},
		}, {
			UID:      41,
			TestType: testSetTimeout,
			Description: `
SetTimeout should work properly when given:
1. bad timeout
`,
			Switches: map[string]bool{
				useBadTimeout: true,
			},
		}, {
			UID:      42,
			TestType: testSetTimeout,
			Description: `
SetTimeout should work properly when given:
1. good timeout
2. preset timeout
`,
			Switches: map[string]bool{
				presetTimeout: true,
			},
		}, {
			UID:      43,
			TestType: testGetTimeout,
			Description: `
GetTimeout should work properly when given:
1. preset timeout
`,
			Switches: map[string]bool{
				presetTimeout: true,
			},
		},
	}
}
