// cmdchain is the chain of command software design pattern algorithm used for
// encapsulating processes and serialize their executions. It is also known
// as "chain of responsibilities" behavioral design pattern.
//
// Likewise, it offers the ability to perform an unified functional calls
// across many functions. With its ability to execute the chained functions
// in serial manner, it allows a full and clear control over the process flow.
//
// "chain of responsibilities" is commonly used for:
//    1. delegate non-relational executions
//    2. network responses
//    3. process piping
//    4. block-process oriented executions
//
package cmdchain
