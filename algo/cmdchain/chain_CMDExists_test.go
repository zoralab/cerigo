package cmdchain

import (
	"testing"
)

func TestCMDExists(t *testing.T) {
	scenarios := testChainScenarios()

	for i, s := range scenarios {
		if s.TestType != testCMDExists {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		c := New()
		_, _, _ = s.prepareChain(c)
		label, expect := s.prepareCMDExists(c)

		// test
		verdict := c.CMDExists(label)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameBool("subject", verdict, "expect", expect)
		s.log(th, map[string]interface{}{
			"input label":    label,
			"expect verdict": expect,
			"got verdict":    verdict,
		})
		th.Conclude()
	}
}
