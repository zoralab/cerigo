package floatconv

// Text is a data structure holding float's subcomponents' value as string.
//
// It has 6 segments that can be re-constructed using String() function.
//   1. Text.Sign         - the float positive/negative sign
//   2. Text.Round        - the round value (XXXX.0)
//   3. Text.Fraction     - the fractional value (0.XXXX)
//   4. Text.Base         - the exponent base number in decimal value
//   5. Text.ExponentSign - the positive/negative sign for exponent value.
//   6. Text.Exponent     - the exponent value (B^(±XXXX) in base10.
type Text struct {
	Sign         string
	Round        string
	Fraction     string
	Base         string
	ExponentSign string
	Exponent     string
}

// String reconstructs the float's subcomponents into a single string output.
//
// Due to the multiple base runes limitations, String() only present the
// [Scientific] format. See "FORMAT" for explanation.
//
//   FORMAT
//   1) Symbol: -dddd.dddd*B^(±dd)
//   2) Fields: [Sign][Round].[Fraction]*[Base]^([ExponentSign][Exponent])
//
//   SYMBOL LEGENDS
//   1) -   = negative sign only. Missing if positive.
//   2) d   = digits (0|1|2|3|4|5|6|7|8|9|a|...|z).
//   3) *   = multiplication sign ( 'x' in mathematics).
//   4) B   = base number representing exponent symbol.
//   5) ±   = positive or negative sign.
//   6) ... = variable length of repeating characters, before and after.
//   7) .   = decimal spot that separates round value and fractional value.
func (t *Text) String() string {
	s := t.Sign + t.Round + "." + t.Fraction
	if t.Exponent != "0" {
		s += "*" + t.Base + "^(" + t.ExponentSign + t.Exponent + ")"
	}

	return s
}

func (t *Text) correction() {
	if t.Round == "" {
		t.Round = "0"
	}

	if t.Fraction == "" {
		t.Fraction = "0"
	}

	if t.Exponent == "" {
		t.Exponent = "0"
	}

	if t.ExponentSign != "-" {
		if t.Exponent != "0" {
			t.ExponentSign = "+"
		}
	}

	if t.Sign != "-" {
		t.Sign = ""
	}
}
