package floatconv

func testConverterScenarios() []testConverterScenario {
	return []testConverterScenario{
		{
			uid:      1,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. large exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      2,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. negative value
2. good round value
3. good fraction value
4. large exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         true,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      3,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. large exponent character
5. negative exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: true,
				useGoodExponent:         true,
			},
		}, {
			uid:      4,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. no exponent character
5. no exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:    false,
				useGoodRound:       true,
				useGoodFraction:    true,
				useCapitalExponent: true,
				useNoExponentSign:  true,
				useGoodExponent:    true,
			},
		}, {
			uid:      5,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value with positive sign
2. good round value
3. good fraction value
4. large exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				usePositiveSign:         true,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      6,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. bad round value
3. good fraction value
4. large exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useBadRound:             true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      7,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. bad fraction value
4. large exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useBadFraction:          true,
				useCapitalExponent:      true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      8,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. bad exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useBadExponent:          true,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      9,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. small exponent character
5. positive exponent sign
6. good exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      false,
				useNegativeExponentSign: false,
				useGoodExponent:         true,
			},
		}, {
			uid:      10,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. large exponent character
5. negative exponent sign
6. adjustable exact exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: true,
				useExactExponent:        true,
			},
		}, {
			uid:      11,
			testType: testParseISO6093,
			description: `
Parse6093 should be able to parse:
1. positive value
2. good round value
3. good fraction value
4. large exponent character
5. negative exponent sign
6. adjustable low exponent value
`,
			switches: map[string]bool{
				useNegativeSign:         false,
				useGoodRound:            true,
				useGoodFraction:         true,
				useCapitalExponent:      true,
				useNegativeExponentSign: true,
				useLowerExponent:        true,
			},
		},
	}
}
