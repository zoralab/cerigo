package floatconv

// Error messages are the list of strings used for generating Error object.
const (
	// ErrorBaseOutOfRange is the given base value is outside of the
	// supported range. It requires user to provide base number within
	// 2 to 36.
	ErrorBaseOutOfRange = "base must be in the range of 2 ≥ b ≥ 36"

	// ErrorConvertBaseNotUsable is the converter is not usable for
	// base conversion. It requires to perform proper parsing again.
	ErrorConvertBaseNotUsable = "unable to convert invalid data"
)
