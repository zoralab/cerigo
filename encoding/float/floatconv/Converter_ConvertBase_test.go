package floatconv

import (
	"testing"
)

func TestConvertBase(t *testing.T) {
	// table-driven approach to test all cases
	scenarios := []struct {
		input         string
		base          int
		normalization int
		precision     int
		expect        string
		err           bool
	}{
		{
			input:         "-12.375e12",
			base:          2,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-1100.011*2^(+40)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          2,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-1100.011*2^(+40)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          8,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-14.3*8^(+13)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          8,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-14.3*8^(+13)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          16,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-c.6*16^(+10)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          16,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-c.6*16^(+10)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          32,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-c.c*32^(+8)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          32,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "-c.c*32^(+8)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          2,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-1.100011*2^(+43)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          2,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-1.100011*2^(+43)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          8,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-1.43*8^(+14)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          8,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-1.43*8^(+14)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          16,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-c.6*16^(+10)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          16,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-c.6*16^(+10)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          32,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-c.c*32^(+8)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          32,
			normalization: NormalizeNormal,
			precision:     50,
			expect:        "-c.c*32^(+8)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          2,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-1100011.0*2^(+37)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          2,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-1100011.0*2^(+37)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          8,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-143.0*8^(+12)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          8,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-143.0*8^(+12)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          16,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-c6.0*16^(+9)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          16,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-c6.0*16^(+9)",
			err:           false,
		}, {
			input:         "-12.375e12",
			base:          32,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-cc.0*32^(+7)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          32,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-cc.0*32^(+7)",
			err:           false,
		}, {
			input:         "0.0e+0",
			base:          2,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "0.0",
			err:           false,
		}, {
			input:         "0",
			base:          2,
			normalization: NormalizeNone,
			precision:     50,
			expect:        "0.0",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          38,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "",
			err:           true,
		}, {
			input:         "-12.375δ+12",
			base:          2,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "",
			err:           true,
		}, {
			input:         "-12.375e+12",
			base:          10,
			normalization: NormalizeRound,
			precision:     50,
			expect:        "-12375.0*10^(+9)",
			err:           false,
		}, {
			input:         "-12.375e+12",
			base:          2,
			normalization: NormalizeRound,
			precision:     -1,
			expect:        "-1100011.0*2^(+37)",
			err:           false,
		}, {
			input:         "12.0",
			base:          2,
			normalization: NormalizeRound,
			precision:     -1,
			expect:        "1100.0",
			err:           false,
		}, {
			input:         "0.375",
			base:          2,
			normalization: NormalizeRound,
			precision:     -1,
			expect:        "11.0*2^(-3)",
			err:           false,
		}, {
			input:         "12.375e-2",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect: "0" +
				".001111110101110000101000111101011100001010001111010111" +
				"*2^(+1)",
			err: false,
		}, {
			input:         "12.0e-1",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect: "1" +
				".001100110011001100110011001100110011001100110011001101",
			err: false,
		}, {
			input:         "12375.0e-2",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect:        "1.111011011*2^(+6)",
			err:           false,
		}, {
			input:         "1.0",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect:        "1.0",
			err:           false,
		}, {
			input:         "12.375e-12",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect: "0" +
				".001111110101110000101000111101011100001010001111010111" +
				"*2^(-9223372036854775808)",
			err: false,
		}, {
			input:         "12375",
			base:          2,
			normalization: NormalizeNormal,
			precision:     -1,
			expect:        "1.1000001010111*2^(+13)",
			err:           false,
		},
	}

	// loop and run each cases
	length := len(scenarios)

	for i, s := range scenarios {
		// prepare
		b := &Converter{}

		_ = b.ParseISO6093(s.input)

		// test
		txt, err := b.ConvertBase(s.base, s.normalization, s.precision)

		// assert

		t.Logf("──────────────────────────────────────────────\n")

		if txt != nil && txt.String() != s.expect {
			t.Errorf("TESTFAIL: mismatched output\n")
		}

		if (err != nil && !s.err) || (err == nil && s.err) {
			t.Errorf("TESTFAIL: mismatched error\n")
		}

		t.Logf("GIVEN :\n")
		t.Logf("input           = %v\n", s.input)
		t.Logf("base            = %v\n", s.base)
		t.Logf("normalization   = %v\n", s.normalization)
		t.Logf("precision       = %v\n", s.precision)
		t.Logf("\n")

		t.Logf("EXPECT:\n")
		t.Logf("output          = %v\n", s.expect)
		t.Logf("error           = %v\n", s.err)
		t.Logf("\n")

		t.Logf("GOT   :\n")

		t.Logf("output          = %v\n", txt)
		t.Logf("error           = %v\n", err)

		if i != length-1 {
			t.Logf("\n")
		}
	}
}
