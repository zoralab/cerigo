package args

import (
	"testing"
)

func TestManagerAdd(t *testing.T) {
	scenarios := testManagerScenarios()

	for i, s := range scenarios {
		if s.TestType != testManagerAdd {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		m := NewManager()
		f, _, _ := s.prepareDataFlags(m)

		// test
		err := runTestManagerAdd(m, f)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.assertFlagExists(th, m, f)
		s.assertError(th, err)
		s.log(th, map[string]interface{}{
			"got manager": m.flags,
			"got error":   err,
			"input flag":  f,
		})
		th.Conclude()
	}
}

func runTestManagerAdd(m *Manager, f *Flag) (err error) {
	defer func() {
		switch e := recover().(type) {
		case error:
			err = e
		default:
			err = nil
		}
	}()
	panic(m.Add(f))
}
