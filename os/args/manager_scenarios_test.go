package args

func testManagerScenarios() []testManagerScenario {
	return []testManagerScenario{
		{
			UID:      1,
			TestType: testManagerAdd,
			Description: `
Add should work properly given a proper flag object`,
			Switches: map[string]bool{
				seekInt: true,
			},
		}, {
			UID:      2,
			TestType: testManagerAdd,
			Description: `
Add should work properly given a missing flag object`,
			Switches: map[string]bool{
				missingFlag: true,
			},
		}, {
			UID:      3,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekInt: true,
			},
		}, {
			UID:      4,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekInt:                true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      5,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekInt:                true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      6,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekInt:             true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      7,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekInt:                        true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      8,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekInt:                        true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      9,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekInt:              true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      10,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekInt:       true,
				prepareNoArgs: true,
			},
		}, {
			UID:      11,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekInt8: true,
			},
		}, {
			UID:      12,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekInt8:               true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      13,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekInt8:               true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      14,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekInt8:            true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      15,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekInt8:                       true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      16,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekInt8:                       true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      17,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekInt8:             true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      18,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int8. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekInt8:      true,
				prepareNoArgs: true,
			},
		}, {
			UID:      19,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekInt16: true,
			},
		}, {
			UID:      20,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekInt16:              true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      21,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekInt16:              true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      22,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekInt16:           true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      23,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekInt16:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      24,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekInt16:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      25,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekInt16:            true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      26,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int16. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekInt16:     true,
				prepareNoArgs: true,
			},
		}, {
			UID:      27,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekInt32: true,
			},
		}, {
			UID:      28,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekInt32:              true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      29,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekInt32:              true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      30,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekInt32:           true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      31,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekInt32:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      32,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekInt32:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      33,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekInt32:            true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      34,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int32. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekInt32:     true,
				prepareNoArgs: true,
			},
		}, {
			UID:      35,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekInt64: true,
			},
		}, {
			UID:      36,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekInt64:              true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      37,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekInt64:              true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      38,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekInt64:           true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      39,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekInt64:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      40,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekInt64:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      41,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekInt64:            true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      42,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Int64. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekInt64:     true,
				prepareNoArgs: true,
			},
		}, {
			UID:      43,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekUInt: true,
			},
		}, {
			UID:      44,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekUInt:               true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      45,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekUInt:               true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      46,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekUInt:            true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      47,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekUInt:                       true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      48,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekUInt:                       true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      49,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekUInt:             true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      50,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekUInt:      true,
				prepareNoArgs: true,
			},
		}, {
			UID:      51,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekUInt8: true,
			},
		}, {
			UID:      52,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekUInt8:              true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      53,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekUInt8:              true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      54,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekUInt8:           true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      55,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekUInt8:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      56,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekUInt8:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      57,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekUInt8:            true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      58,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt8. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekUInt:      true,
				prepareNoArgs: true,
			},
		}, {
			UID:      59,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekUInt16: true,
			},
		}, {
			UID:      60,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekUInt16:             true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      61,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekUInt16:             true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      62,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekUInt16:          true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      63,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekUInt16:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      64,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekUInt16:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      65,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekUInt16:           true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      66,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt16. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekUInt16:    true,
				prepareNoArgs: true,
			},
		}, {
			UID:      67,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekUInt32: true,
			},
		}, {
			UID:      68,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekUInt32:             true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      69,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekUInt32:             true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      70,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekUInt32:          true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      71,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekUInt32:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      72,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekUInt32:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      73,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekUInt32:           true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      74,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt32. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekUInt32:    true,
				prepareNoArgs: true,
			},
		}, {
			UID:      75,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekUInt64: true,
			},
		}, {
			UID:      76,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekUInt64:             true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      77,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekUInt64:             true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      78,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekUInt64:          true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      79,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekUInt64:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      80,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekUInt64:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      81,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekUInt64:           true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      82,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking UInt64. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekUInt64:    true,
				prepareNoArgs: true,
			},
		}, {
			UID:      83,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekFloat32: true,
			},
		}, {
			UID:      84,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekFloat32:            true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      85,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekFloat32:            true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      86,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekFloat32:         true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      87,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekFloat32:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      88,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekFloat32:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      89,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekFloat32:          true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      90,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float32. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekFloat32:   true,
				prepareNoArgs: true,
			},
		}, {
			UID:      91,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekFloat64: true,
			},
		}, {
			UID:      92,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekFloat64:            true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      93,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekFloat64:            true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      94,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekFloat64:         true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      95,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekFloat64:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      96,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekFloat64:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      97,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekFloat64:          true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      98,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Float64. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekFloat64:   true,
				prepareNoArgs: true,
			},
		}, {
			UID:      99,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekString: true,
			},
		}, {
			UID:      100,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekString:             true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      101,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekString:             true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      102,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekString:          true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      103,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekString:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      104,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekString:                     true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      105,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekString:           true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      106,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking String. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekString:    true,
				prepareNoArgs: true,
			},
		}, {
			UID:      107,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekBoolean: true,
			},
		}, {
			UID:      108,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekBoolean:            true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      109,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekBoolean:            true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      110,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekBoolean:         true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      111,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekBoolean:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      112,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekBoolean:                    true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      113,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekBoolean:          true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      114,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Boolean. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekBoolean:   true,
				prepareNoArgs: true,
			},
		}, {
			UID:      115,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekBytes: true,
			},
		}, {
			UID:      116,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekBytes:              true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      117,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekBytes:              true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      118,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekBytes:           true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      119,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekBytes:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      120,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekBytes:                      true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      121,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program arg`,
			Switches: map[string]bool{
				seekBytes:            true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      122,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking Bytes. The argument
pattern is "./program`,
			Switches: map[string]bool{
				seekBytes:     true,
				prepareNoArgs: true,
			},
		}, {
			UID:      123,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program --arg 123`,
			Switches: map[string]bool{
				seekBytesPointer: true,
			},
		}, {
			UID:      124,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program -a 123`,
			Switches: map[string]bool{
				seekBytesPointer:       true,
				prepareShortArgKeyword: true,
			},
		}, {
			UID:      125,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program -a=123`,
			Switches: map[string]bool{
				seekBytesPointer:       true,
				prepareShortArgKeyword: true,
				prepareArgWithEqual:    true,
			},
		}, {
			UID:      126,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program --arg=123"`,
			Switches: map[string]bool{
				seekBytesPointer:    true,
				prepareArgWithEqual: true,
			},
		}, {
			UID:      127,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program --arg="123"`,
			Switches: map[string]bool{
				seekBytesPointer:               true,
				prepareArgWithEqual:            true,
				prepareArgValueWithDoubleQuote: true,
			},
		}, {
			UID:      128,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program --arg='123'`,
			Switches: map[string]bool{
				seekBytesPointer:               true,
				prepareArgWithEqual:            true,
				prepareArgValueWithSingleQuote: true,
			},
		}, {
			UID:      129,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program arg`,
			Switches: map[string]bool{
				seekBytesPointer:     true,
				prepareStandaloneArg: true,
			},
		}, {
			UID:      130,
			TestType: testManagerParse,
			Description: `
Parse should work properly given a proper flag seeking BytesPointer. The
argument pattern is "./program`,
			Switches: map[string]bool{
				seekBytesPointer: true,
				prepareNoArgs:    true,
			},
		}, {
			UID:      131,
			TestType: testManagerAdd,
			Description: `
Add should work properly given a pre-existed flag object`,
			Switches: map[string]bool{
				preExistedFlag: true,
			},
		}, {
			UID:      132,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to false
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to true
`,
			Switches: map[string]bool{},
		}, {
			UID:      133,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to false
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				hideFlagExamples: true,
			},
		}, {
			UID:      134,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				disableFlagHelp: true,
			},
		}, {
			UID:      135,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 empty label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				emptyLabelsList: true,
			},
		}, {
			UID:      136,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 empty label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper unicode description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				unicodeProgramDescription: true,
			},
		}, {
			UID:      137,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper unicode description
     2.3 proper version
     2.4 empty examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				emptyProgramExamples: true,
			},
		}, {
			UID:      138,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 empty label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 empty description
     2.3 proper version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				emptyProgramDescription: true,
			},
		}, {
			UID:      139,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 integer version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				intProgramVersion: true,
			},
		}, {
			UID:      140,
			TestType: testManagerPrintHelp,
			Description: `
PrintHelp should work properly given:
  1. a set of flags with:
     1.1 proper label
     1.2 proper examples
     1.3 proper value label
     1.4 proper value pointer
     1.5 proper help statement
     1.6 disable help set to true
  2. manager with:
     2.1 proper name
     2.2 proper description
     2.3 unsigned integer version
     2.4 proper examples
     2.5 show flag examples set to false
`,
			Switches: map[string]bool{
				uintProgramVersion: true,
			},
		},
	}
}
