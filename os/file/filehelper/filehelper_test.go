package filehelper

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	filepath1      = "./sample1.txt"
	filepath2      = "./sample2.txt"
	filepath3      = "./sample3.txt"
	filepath4      = "./sample4.txt"
	badFilepath    = "/bad_sample.txt"
	seeker1        = "./seeker1.txt"
	seeker2        = "./seeker2.txt"
	seeker1Content = "a quick brown fox jumps over the lazy dog"
	seeker2Content = "这是一场无法得来的机会，冲啊！！！！"
	seekerKeyword  = "fox"
	seekerKeywords = "brown fox cat"
	badKeywords    = "grey cat laser"
)

const (
	testCompare         = "testCompare"
	testFileExists      = "testFileExists"
	testFileHasKeywords = "testFileHasKeywords"
)

const (
	badFilepath1          = "badFilepath1"
	badFilepath2          = "badFilepath2"
	badSeekerKeywords     = "badSeekerKeywords"
	differentFileContent  = "differentFileContent"
	differentFileLength   = "differentFileLength"
	highChunkSize         = "highChunkSize"
	highStandardChunkSize = "highStandardChunkSize"
	lowChunkSize          = "lowChunkSize"
	emptyFilepath1        = "emptyFilepath1"
	emptySeekerKeywords   = "emptySeekerKeywords"
	noPermissionFilepath1 = "noPermissionFilepath1"
	noPermissionFilepath2 = "noPermissionFilepath2"
	sameFile              = "sameFile"
	singleSeekerKeyword   = "singleSeekerKeyword"
	useDeepCompare        = "useDeepCompare"
)

type fileHelperScenario thelper.Scenario

func (s *fileHelperScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *fileHelperScenario) assertVerdict(th *thelper.THelper,
	verdict bool,
	ret bool) {
	if ret != verdict {
		th.Errorf("mismatched verdict")
	}
}

func (s *fileHelperScenario) assertFileHasKeywords(th *thelper.THelper,
	ret []int,
	expects []int) {
	if ret == nil {
		th.Errorf("ret was returned nil")
		return
	}

	if expects == nil {
		th.Errorf("test expected list was nil")
		return
	}

	if len(ret) != len(expects) {
		th.Errorf("mismatched returned list length")
		return
	}

	for i, v := range ret {
		if v != expects[i] {
			th.Errorf("bad detection list")
		}
	}
}

func (s *fileHelperScenario) prepareTestHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *fileHelperScenario) prepareFileHelper(f *FileHelper) {
	switch {
	case s.Switches[lowChunkSize]:
		f.ChunkSize = 2
	case s.Switches[highChunkSize]:
		f.ChunkSize = 123456
	case s.Switches[highStandardChunkSize]:
		f.ChunkSize = Byte128K
	default:
		f.ChunkSize = 0
	}
}

func (s *fileHelperScenario) prepareFiles() (f1 string, f2 string, v bool) {
	f1 = filepath1
	f2 = filepath3
	v = true

	s.restoreSampleFilesPermission()

	switch {
	case s.Switches[differentFileContent]:
		f2 = filepath4
		v = false
	case s.Switches[differentFileLength]:
		f2 = filepath2
		v = false
	case s.Switches[sameFile]:
		f2 = filepath1
	case s.Switches[badFilepath1]:
		f1 = badFilepath
		v = false
	case s.Switches[badFilepath2]:
		f2 = badFilepath
		v = false
	case s.Switches[noPermissionFilepath1]:
		v = false

		s.setSampleFilesPermission(filepath1)
	case s.Switches[noPermissionFilepath2]:
		v = false

		s.setSampleFilesPermission(filepath3)
	}

	return f1, f2, v
}

func (s *fileHelperScenario) runCompare(deep func(string, string) bool,
	standard func(string, string) bool,
	f1 string,
	f2 string) bool {
	if s.Switches[useDeepCompare] {
		return deep(f1, f2)
	}

	return standard(f1, f2)
}

func (s *fileHelperScenario) setSampleFilesPermission(path string) {
	_ = os.Chmod(path, 0222)
}

func (s *fileHelperScenario) restoreSampleFilesPermission() {
	restoreSampleFilesPermission()
}

func (s *fileHelperScenario) prepareFile() (f1 string, verdict bool) {
	f1 = filepath1
	verdict = true

	if s.Switches[badFilepath1] {
		f1 = badFilepath
		verdict = false
	}

	return f1, verdict
}

func (s *fileHelperScenario) prepareSeekerFiles() (f1 string,
	keywords []string,
	expects []int) {
	f1 = seeker1
	k := seekerKeywords
	expects = []int{1}

	switch {
	case s.Switches[emptyFilepath1]:
		f1 = ""
		expects = []int{}
	case s.Switches[badFilepath1]:
		f1 = badFilepath
		expects = []int{}
	case s.Switches[differentFileContent]:
		f1 = seeker2
		expects = []int{}
	}

	switch {
	case s.Switches[badSeekerKeywords]:
		k = badKeywords
		expects = []int{}
	case s.Switches[emptySeekerKeywords]:
		k = ""
		expects = []int{}
	case s.Switches[singleSeekerKeyword]:
		k = seekerKeyword
	}

	keywords = strings.Split(k, " ")

	return f1, keywords, expects
}
