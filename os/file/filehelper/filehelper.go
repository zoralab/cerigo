package filehelper

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"strings"
)

const (
	// Byte1K is the 1k ChunkSize
	Byte1K = uint(1000)

	// Byte4K is the 4k ChunkSize
	Byte4K = uint(4000)

	// Byte8K is the 8k ChunkSize
	Byte8K = uint(8000)

	// Byte64K is the 64k ChunkSize
	Byte64K = uint(64000)

	// Byte128K is the 128k ChunkSize
	Byte128K = uint(128000)
)

// FileHelper is a structure for hosting helper functions related to files. It
// has a public element:
//   1. ChunkSize - setting for read/write in packet size. The recommended
//                  value is Byte64K, depending on the computational
//                  availability. You can set any values at your will. If
//                  nothing is set, it uses Byte4K as its default chunk size.
type FileHelper struct {
	ChunkSize uint
}

func (f *FileHelper) makeChunkSizeUsable() {
	if f.ChunkSize == 0 {
		f.ChunkSize = Byte4K
	}
}

// Compare is to perform byte-to-byte comparison for 2 given files. This
// function uses low memory deep compare algorithm to perform file comparison.
// It compares bytes by bytes between the files until the first byte differences
// is found. In that case, it returns the verdict immediately.
//
// All being said, this function by itself is not suitable for cryptography
// requirements since the comparison timing may introduce side-channel timing
// attack.
//
// Therefore, this function can perform slowly if either of the files are very
// large (due to algorithm).
//
// It returns:
//   1. true          - both filepaths are having the same contents
//   2. false         - both filepaths are not having the same contents
func (f *FileHelper) Compare(path1 string, path2 string) (verdict bool) {
	if f.SameStat(path1, path2) {
		return true
	}

	if !f.SimilarStat(path1, path2) {
		return false
	}

	return f.deepCompare(path1, path2)
}

func (f *FileHelper) deepCompare(path1, path2 string) (verdict bool) {
	f1, err := os.Open(path1)
	if err != nil {
		return verdict
	}
	defer f1.Close()

	f2, err := os.Open(path2)
	if err != nil {
		return verdict
	}
	defer f2.Close()

	f.makeChunkSizeUsable()

	for {
		b1 := make([]byte, f.ChunkSize)
		_, err1 := f1.Read(b1)
		b2 := make([]byte, f.ChunkSize)
		_, err2 := f2.Read(b2)

		if err1 != nil || err2 != nil {
			if err1 == io.EOF && err2 == io.EOF {
				verdict = true
			}

			return verdict
		}

		if !bytes.Equal(b1, b2) {
			return verdict
		}
	}
}

// SameStat checks the given two files having the same inode stat. If it
// does, it will return true, which means both filepaths are referring to the
// same file. Only filepath that fulfilles os.SameFile works here.
//
// It returns:
//   1. true  - both filepaths are referring to the same file.
//   2. false - the filepaths are not referring to the same file.
func (f *FileHelper) SameStat(path1 string, path2 string) (verdict bool) {
	fi1, err := os.Stat(path1)
	if err != nil {
		return false
	}

	fi2, err := os.Stat(path2)
	if err != nil {
		return false
	}

	if os.SameFile(fi1, fi2) {
		return true
	}

	return false
}

// SimilarStat checks the given two paths are having similar file pattern such
// as its directory/file nature, size and modes. It excludes modification time
// and name for further comparison. This function only evaluates the following
// aspects:
//   1. Same size
//   2. Same mode (file's mode and permission bits - os.FileMode)
//   3. Same type (directory / file)
//
// SimilarStat will always return false for anything else, including error
// situations.
func (f *FileHelper) SimilarStat(path1 string, path2 string) (verdict bool) {
	fi1, err := os.Stat(path1)
	if err != nil {
		return false
	}

	fi2, err := os.Stat(path2)
	if err != nil {
		return false
	}

	if fi1.Size() == fi2.Size() &&
		fi1.IsDir() == fi2.IsDir() &&
		fi1.Mode() == fi2.Mode() {
		return true
	}

	return false
}

// FileExists is to check whether a file/directory exists
func (f *FileHelper) FileExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// FileHasKeywords is to check whether a keyword exists in the file.
// It takes inputs of:
//   1. path              - path to file
//   2. keywords          - the keywords. It can be 1 or many
//
// It returns:
//   1. empty list        - problem opening the file or no results found.
//   2. numbers list      - an array of line numbers with matched keyword(s).
func (f *FileHelper) FileHasKeywords(path string,
	keywords ...string) (list []int) {
	list = []int{}

	// validate user's input
	i := len(keywords)
	if i <= 0 || keywords[0] == "" {
		return list
	}

	// opening file
	f1, err := os.Open(path)
	if err != nil {
		return list
	}
	defer f1.Close()

	// setting up scanner
	scanner := bufio.NewScanner(f1)
	line := 1
	seeker := f.singleKeywordScan

	if i > 1 {
		seeker = f.multipleKeywordScan
	}

	// scan
	for scanner.Scan() {
		if seeker(scanner.Text(), keywords) {
			list = append(list, line)
		}
		line++
	}

	// close
	return list
}

func (f *FileHelper) singleKeywordScan(line string, keywords []string) bool {
	return strings.Contains(line, keywords[0])
}

func (f *FileHelper) multipleKeywordScan(line string, keywords []string) bool {
	for _, keyword := range keywords {
		if strings.Contains(line, keyword) {
			return true
		}
	}

	return false
}
