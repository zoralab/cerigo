package filehelper

import (
	"testing"
)

func TestFileExists(t *testing.T) {
	scenarios := testFileHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testFileExists {
			continue
		}

		// prepare
		th := s.prepareTestHelper(t)
		f := &FileHelper{}
		s.prepareFileHelper(f)
		f1, isExists := s.prepareFile()

		// test
		v := f.FileExists(f1)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		s.log(th, map[string]interface{}{
			"filepath1":      f1,
			"verdict":        v,
			"expect verdict": isExists,
		})
		th.Conclude()
	}
}
