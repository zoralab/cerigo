package term

func testTerminalScenarios() []testTerminalScenario {
	return []testTerminalScenario{
		{
			UID:      1,
			TestType: testNewTerminal,
			Description: `
NewTerminal should work properly when given a proper TerminalType value`,
			Switches: map[string]bool{},
		}, {
			UID:      2,
			TestType: testNewTerminal,
			Description: `
NewTerminal should work properly when given a unknown TerminalType value`,
			Switches: map[string]bool{
				unknownTerminalType: true,
			},
		}, {
			UID:      3,
			TestType: testNewTerminal,
			Description: `
NewTerminal should work properly when given a BASH TerminalType value`,
			Switches: map[string]bool{
				useBASHTerminal: true,
			},
		}, {
			UID:      4,
			TestType: testNewTerminal,
			Description: `
NewTerminal should work properly when given a SH TerminalType value`,
			Switches: map[string]bool{
				useSHTerminal: true,
			},
		}, {
			UID:      5,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a proper statusID, a proper format,
a proper arguments`,
			Switches: map[string]bool{},
		}, {
			UID:      6,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a info statusID, a proper format, a
proper arguments`,
			Switches: map[string]bool{
				useInfoStatus: true,
			},
		}, {
			UID:      7,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a warning statusID, a proper
format, a proper arguments`,
			Switches: map[string]bool{
				useWarningStatus: true,
			},
		}, {
			UID:      8,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a error statusID, a proper format,
a proper arguments`,
			Switches: map[string]bool{
				useErrorStatus: true,
			},
		}, {
			UID:      9,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a debug statusID, a proper format,
a proper arguments`,
			Switches: map[string]bool{
				useDebugStatus: true,
			},
		}, {
			UID:      10,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given an unknown statusID, a proper
format, a proper arguments`,
			Switches: map[string]bool{
				useUnknownStatus: true,
			},
		}, {
			UID:      11,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a proper statusID, a proper format,
a proper arguments when stderr is nil`,
			Switches: map[string]bool{
				missingSTDERR: true,
			},
		}, {
			UID:      12,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a proper statusID, a missing
format, a proper arguments`,
			Switches: map[string]bool{
				missingFormat: true,
			},
		}, {
			UID:      13,
			TestType: testPrintStatus,
			Description: `
PrintStatus should work properly when given a proper statusID, a proper format,
a missing arguments`,
			Switches: map[string]bool{
				missingArguments: true,
			},
		}, {
			UID:      14,
			TestType: testPrintStatus,
			Description: `PrintStatus should work properly when
given a proper statusID, a missing format, a missing arguments`,
			Switches: map[string]bool{
				missingFormat:    true,
				missingArguments: true,
			},
		}, {
			UID:      15,
			TestType: testPrintf,
			Description: `
Printf should work properly when given a proper statusID, a proper format, a
proper arguments`,
			Switches: map[string]bool{},
		}, {
			UID:      16,
			TestType: testPrintf,
			Description: `
Printf should work properly when given a proper statusID, a proper format, a
proper arguments when stdout is nil`,
			Switches: map[string]bool{
				missingSTDOUT: true,
			},
		}, {
			UID:      17,
			TestType: testPrintf,
			Description: `
Printf should work properly when given a proper statusID, a missing format, a
proper arguments`,
			Switches: map[string]bool{
				missingFormat: true,
			},
		}, {
			UID:      18,
			TestType: testPrintf,
			Description: `
Printf should work properly when given a proper statusID, a proper format, a
missing arguments`,
			Switches: map[string]bool{
				missingArguments: true,
			},
		}, {
			UID:      19,
			TestType: testPrintf,
			Description: `
Printf should work properly when given a proper statusID, a missing format, a
proper arguments`,
			Switches: map[string]bool{
				missingFormat:    true,
				missingArguments: true,
			},
		}, {
			UID:      20,
			TestType: testAdd,
			Description: `
Add should work properly when given a proper label, and a proper command`,
			Switches: map[string]bool{},
		}, {
			UID:      21,
			TestType: testAdd,
			Description: `
Add should return error when given an empty label`,
			Switches: map[string]bool{
				useEmptyLabel: true,
			},
		}, {
			UID:      22,
			TestType: testAdd,
			Description: `
Add should work properly when given a proper label, and an empty command`,
			Switches: map[string]bool{
				useEmptyCommand: true,
			},
		}, {
			UID:      23,
			TestType: testAdd,
			Description: `
Add should work properly when given a proper label, and a bad command`,
			Switches: map[string]bool{
				useEmptyCommand: true,
			},
		}, {
			UID:      24,
			TestType: testAdd,
			Description: `
Add should return error when given the command is already exists`,
			Switches: map[string]bool{
				preOccupyCommand: true,
			},
		}, {
			UID:      25,
			TestType: testRegister,
			Description: `
Register should work properly when given a proper label, and a proper command`,
			Switches: map[string]bool{},
		}, {
			UID:      26,
			TestType: testRegister,
			Description: `
Register should return error when given an empty label`,
			Switches: map[string]bool{
				useEmptyLabel: true,
			},
		}, {
			UID:      27,
			TestType: testRegister,
			Description: `
Register should work properly when given a proper label, and an empty command`,
			Switches: map[string]bool{
				useEmptyCommand: true,
			},
		}, {
			UID:      28,
			TestType: testRegister,
			Description: `
Register should work properly when given a proper label, and a bad command`,
			Switches: map[string]bool{
				useEmptyCommand: true,
			},
		}, {
			UID:      29,
			TestType: testRegister,
			Description: `
Register should return error when given the command is already exists`,
			Switches: map[string]bool{
				preOccupyCommand: true,
			},
		}, {
			UID:      30,
			TestType: testGet,
			Description: `
Get should return command when given the command exists and a correct label`,
			Switches: map[string]bool{
				preOccupyCommand: true,
			},
		}, {
			UID:      31,
			TestType: testGet,
			Description: `
Get should return nil when given the command exists and an empty label`,
			Switches: map[string]bool{
				preOccupyCommand: true,
				useEmptyLabel:    true,
			},
		}, {
			UID:      32,
			TestType: testUpdate,
			Description: `
Update should work properly when given the command exists, a proper label, and
a new command`,
			Switches: map[string]bool{},
		}, {
			UID:      33,
			TestType: testUpdate,
			Description: `
Update should return error when given the command exists, an empty label, and a
new command`,
			Switches: map[string]bool{
				useEmptyLabel: true,
			},
		}, {
			UID:      34,
			TestType: testUpdate,
			Description: `
Update should return error when given the command is exists, a bad label, and a
new command`,
			Switches: map[string]bool{
				useBadLabel: true,
			},
		}, {
			UID:      35,
			TestType: testDelete,
			Description: `
Delete should work properly when given the command exists, and a proper label`,
			Switches: map[string]bool{
				preOccupyCommand: true,
			},
		}, {
			UID:      36,
			TestType: testDelete,
			Description: `
Delete should work properly when given the command is missing, and a proper
label`,
			Switches: map[string]bool{
				preOccupyCommand: true,
			},
		}, {
			UID:      37,
			TestType: testDelete,
			Description: `
Delete should work properly when given the command exists, and an empty label`,
			Switches: map[string]bool{
				preOccupyCommand: true,
				useEmptyLabel:    true,
			},
		}, {
			UID:      38,
			TestType: testRun,
			Description: `
Run should work properly when given a short execution command exists, a proper
label, set to wait, use BASH terminal`,
			Switches: map[string]bool{
				useBASHTerminal: true,
			},
		}, {
			UID:      39,
			TestType: testRun,
			Description: `
Run should work properly when given a short execution command exists, a proper
label, set to wait, use SH terminal`,
			Switches: map[string]bool{
				useSHTerminal: true,
			},
		}, {
			UID:      40,
			TestType: testRun,
			Description: `
Run should work properly when given a short execution command exists, a proper
label, set to wait, use DOS terminal`,
			Switches: map[string]bool{
				useDOSTerminal: true,
			},
		}, {
			UID:      41,
			TestType: testRun,
			Description: `
Run should work properly when given a short execution command exists, a proper
label, set to wait, use unknown terminal`,
			Switches: map[string]bool{
				unknownTerminalType: true,
			},
		}, {
			UID:      42,
			TestType: testRun,
			Description: `
Run should work properly when given a long execution command exists, a proper
label, set to wait, use BASH terminal`,
			Switches: map[string]bool{
				useShortTimeout: true,
				useLongCommand:  true,
				useBASHTerminal: true,
			},
		}, {
			UID:      43,
			TestType: testRun,
			Description: `
Run should work properly when given a long execution command exists, a proper
label, set to wait, use SH terminal`,
			Switches: map[string]bool{
				useShortTimeout: true,
				useLongCommand:  true,
				useSHTerminal:   true,
			},
		}, {
			UID:      44,
			TestType: testRun,
			Description: `
Run should work properly when given a long execution command exists, a proper
label, set to wait, use DOS terminal`,
			Switches: map[string]bool{
				useShortTimeout: true,
				useLongCommand:  true,
				useDOSTerminal:  true,
			},
		}, {
			UID:      45,
			TestType: testRun,
			Description: `
Run should work properly when given a long execution command exists, a proper
label, set to wait, use unknown terminal`,
			Switches: map[string]bool{
				useShortTimeout:     true,
				useLongCommand:      true,
				unknownTerminalType: true,
			},
		}, {
			UID:      46,
			TestType: testRun,
			Description: `
Run should return error when given a short execution command exists, an empty
label, set to wait, use BASH terminal`,
			Switches: map[string]bool{
				useEmptyLabel:   true,
				useBASHTerminal: true,
			},
		}, {
			UID:      47,
			TestType: testRun,
			Description: `
Run should return error when given a short command exists, an empty label, set
to wait, use SH terminal`,
			Switches: map[string]bool{
				useEmptyLabel: true,
				useSHTerminal: true,
			},
		}, {
			UID:      48,
			TestType: testRun,
			Description: `
Run should return error when given a short command exists, an empty label, set
to wait, use DOS terminal`,
			Switches: map[string]bool{
				useEmptyLabel:  true,
				useDOSTerminal: true,
			},
		}, {
			UID:      49,
			TestType: testRun,
			Description: `
Run should return error when given a short command exists, an empty label, set
to wait, use unknown terminal`,
			Switches: map[string]bool{
				useEmptyLabel:       true,
				unknownTerminalType: true,
			},
		}, {
			UID:      50,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
wait, use BASH terminal`,
			Switches: map[string]bool{
				useBASHTerminal: true,
			},
		}, {
			UID:      51,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
wait, use SH terminal`,
			Switches: map[string]bool{
				useBadCommand: true,
				useSHTerminal: true,
			},
		}, {
			UID:      52,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
wait, use DOS terminal`,
			Switches: map[string]bool{
				useBadCommand:  true,
				useDOSTerminal: true,
			},
		}, {
			UID:      53,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
wait, use unknown terminal`,
			Switches: map[string]bool{
				useBadCommand:       true,
				unknownTerminalType: true,
			},
		}, {
			UID:      54,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
no wait, use BASH terminal`,
			Switches: map[string]bool{
				doNotWait:       true,
				useBASHTerminal: true,
			},
		}, {
			UID:      55,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
no wait, use SH terminal`,
			Switches: map[string]bool{
				doNotWait:     true,
				useSHTerminal: true,
			},
		}, {
			UID:      56,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
no wait, use DOS terminal`,
			Switches: map[string]bool{
				doNotWait:      true,
				useDOSTerminal: true,
			},
		}, {
			UID:      57,
			TestType: testRun,
			Description: `
Run should return error when given a bad command exists, a proper label, set to
no wait, use unknown terminal`,
			Switches: map[string]bool{
				doNotWait:           true,
				unknownTerminalType: true,
			},
		}, {
			UID:      58,
			TestType: testExecute,
			Description: `
Execute should run properly when given a proper command, a proper timeout,
using BASH terminal`,
			Switches: map[string]bool{
				useBASHTerminal: true,
			},
		}, {
			UID:      59,
			TestType: testExecute,
			Description: `
Execute should run properly when given a proper command, a short timeout, using
BASH terminal`,
			Switches: map[string]bool{
				useBASHTerminal: true,
				useShortTimeout: true,
			},
		}, {
			UID:      60,
			TestType: testExecute,
			Description: `
Execute should run properly when given a proper command, a short timeout, using
BASH terminal`,
			Switches: map[string]bool{
				useBASHTerminal: true,
				useLongCommand:  true,
				useShortTimeout: true,
			},
		}, {
			UID:      61,
			TestType: testIsRoot,
			Description: `
IsRoot should run properly when root is unset`,
			Switches: map[string]bool{},
		}, {
			UID:      62,
			TestType: testIsRoot,
			Description: `
IsRoot should run properly when root is set`,
			Switches: map[string]bool{
				setRoot: true,
			},
		}, {
			UID:      63,
			TestType: testSize,
			Description: `
Size should run properly when using in syscall`,
			Switches: map[string]bool{
				useSystemTermSizes: true,
			},
		}, {
			UID:      64,
			TestType: testSize,
			Description: `
Size should run properly when using test values`,
			Switches: map[string]bool{},
		},
	}
}
