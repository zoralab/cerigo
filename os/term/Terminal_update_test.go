package term

import (
	"testing"
)

func TestUpdate(t *testing.T) {
	scenarios := testTerminalScenarios()

	for i, s := range scenarios {
		if s.TestType != testUpdate {
			continue
		}

		th := s.prepareTHelper(t)
		// prepare
		x := NewTerminal(s.prepareTerminalType())
		s.prepareTerminal(x)
		label, command, hasError := s.prepareCommand(x)
		timeout, hasError := s.prepareTimeout(hasError)
		c := s.prepareNewCommand()

		// test
		err := x.Update(label, c, timeout)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectError(err, hasError)
		s.assertUpdatedCommand(th, x, label, c)
		cmd := x.Get(label)
		outC := ""

		if cmd != nil {
			outC = cmd.command
		}

		s.log(th, map[string]interface{}{
			"terminal":     x,
			"inLabel":      label,
			"inCommand":    command,
			"inNewCommand": c,
			"outCommand":   outC,
			"outError":     err,
			"expectError":  hasError,
		})

		th.Conclude()
	}
}
