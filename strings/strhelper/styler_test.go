package strhelper

import (
	"testing"

	"gitlab.com/zoralab/cerigo/testing/thelper"
)

const (
	sampleN1 = `"a quick brownFox jumps \r\n \r \n over the lazyDog"`
	sampleN2 = `
This is the first paragraph. It has \r\n and \r words.
I will however write as long as I want. SuperblyGoodAndLongWishes!
`
	jointSentences = "is a phrase to test all ASCII characters"
)

const (
	testToUNIX      = "testToUNIX"
	testToWindows   = "testToWindows"
	testWordWrap    = "testWordWrap"
	testContentWrap = "testContentWrap"
	testIndent      = "testIndent"
)

const (
	expectCR         = "expectCR"
	expectCRLF       = "expectCRLF"
	expectLF         = "expectLF"
	useBadIndent     = "useBadIndent"
	useBadIndentSize = "useBadIndentSize"
	useBadLimit      = "useBadLimit"
	useBadParagraph  = "useBadParagraph"
	useBadSample     = "useBadSample"
	useCR            = "useCR"
	useCRLF          = "useCRLF"
	useLF            = "useLF"
	useTabIndent     = "useTabIndent"
	useWeirdEOL      = "useWeirdEOL"
)

type testStrHelperScenario thelper.Scenario

func (s *testStrHelperScenario) log(th *thelper.THelper,
	data map[string]interface{}) {
	th.LogScenario(thelper.Scenario(*s), data)
}

func (s *testStrHelperScenario) prepareTHelper(t *testing.T) *thelper.THelper {
	return thelper.NewTHelper(t)
}

func (s *testStrHelperScenario) prepareString() (sample, expect string) {
	switch {
	case s.Switches[useCRLF]:
		sample = s.constructStrings(useCRLF)
	case s.Switches[useCR]:
		sample = s.constructStrings(useCR)
	case s.Switches[useLF]:
		fallthrough
	default:
		sample = s.constructStrings(useLF)
	}

	switch {
	case s.Switches[expectCR]:
		expect = s.constructStrings(useCR)
	case s.Switches[expectLF]:
		expect = s.constructStrings(useLF)
	case s.Switches[expectCRLF]:
		fallthrough
	default:
		expect = s.constructStrings(useCRLF)
	}

	return sample, expect
}

func (s *testStrHelperScenario) constructStrings(eolType string) (out string) {
	eol := ""

	switch eolType {
	case useCRLF:
		eol = CharWindowsNewLine
	case useCR:
		eol = CharLegacyNewLine
	default:
		eol = CharUNIXNewLine
	}

	out = sampleN1 + eol + jointSentences + eol

	return out
}

func (s *testStrHelperScenario) prepareParagraph() (sample string,
	limit uint,
	expect []string) {
	limit = 15
	sample = sampleN2
	expect = []string{
		"This is the",
		"first",
		"paragraph. It", "has \\r\\n and",
		"\\r words. I",
		"will however",
		"write as long",
		"as I want.",
		"SuperblyGoodAndLongWishes!",
	}

	switch {
	case s.Switches[useBadSample]:
		sample = ""
		expect = []string{}
	case s.Switches[useBadLimit]:
		limit = 0
		expect = nil
	}

	return sample, limit, expect
}

func (s *testStrHelperScenario) prepareParagraphsSample() (sample string,
	limit uint,
	eol string,
	expect []string) {
	limit = 15
	eol = ""

	switch {
	case s.Switches[useCRLF]:
		eol = CharWindowsNewLine
	case s.Switches[useCR]:
		eol = CharLegacyNewLine
	default:
		eol = CharUNIXNewLine
	}

	switch {
	case s.Switches[expectCR]:
		expect = []string{
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and",
			"\\r words.",
			"\r",
			"\r",
			"I will",
			"however write",
			"as long as I", "want.",
			"SuperblyGoodAndLongWishes!",
			"\r",
			"\r",
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and", "\\r words.",
			"\r",
			"\r",
			"I will",
			"however write",
			"as long as I",
			"want.",
			"SuperblyGoodAndLongWishes!",
			"\r",
		}
	case s.Switches[expectLF]:
		expect = []string{
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and",
			"\\r words.",
			"\n",
			"\n",
			"I will",
			"however write",
			"as long as I",
			"want.",
			"SuperblyGoodAndLongWishes!",
			"\n",
			"\n",
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and",
			"\\r words.",
			"\n",
			"\n",
			"I will",
			"however write",
			"as long as I",
			"want.",
			"SuperblyGoodAndLongWishes!",
			"\n",
		}
	case s.Switches[expectCRLF]:
		fallthrough
	default:
		expect = []string{
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and",
			"\\r words.",
			"\r\n",
			"\r\n",
			"I will",
			"however write",
			"as long as I",
			"want.",
			"SuperblyGoodAndLongWishes!",
			"\r\n",
			"\r\n",
			"This is the",
			"first",
			"paragraph. It",
			"has \\r\\n and",
			"\\r words.",
			"\r\n",
			"\r\n",
			"I will",
			"however write",
			"as long as I",
			"want.",
			"SuperblyGoodAndLongWishes!",
			"\r\n",
		}
	}

	sample = sampleN2 + eol + sampleN2

	switch {
	case s.Switches[useWeirdEOL]:
		eol = "|whatever|"
		sample = sampleN2 + eol + sampleN2
		expect = nil
	case s.Switches[useBadSample]:
		sample = ""
		expect = []string{}
	case s.Switches[useBadLimit]:
		limit = 0
		expect = nil
	}

	return sample, limit, eol, expect
}

func (s *testStrHelperScenario) prepareIndentSample() (sample []string,
	indentChar string,
	indentSize uint,
	expect []string) {
	sample = []string{
		"This is the",
		"first",
		"paragraph. It",
		"has \\r\\n and",
		"\\r words.",
		"\n",
		"\r\n",
		"I will",
		"however write",
		"as long as I",
		"want.",
		"SuperblyGoodAndLongWishes!",
		"\r",
		"\r\n",
		"This is the",
		"first",
		"paragraph. It",
		"has \\r\\n and",
		"\\r words.",
		"\n",
		"\r\n",
		"I will",
		"however write",
		"as long as I",
		"want.",
		"SuperblyGoodAndLongWishes!",
		"\r",
		"\r\n",
	}

	switch {
	case s.Switches[useBadParagraph]:
		sample = []string{}
		expect = []string{}
	case s.Switches[useBadIndentSize]:
		indentSize = 0
		expect = nil
	case s.Switches[useBadIndent]:
		indentChar = "blablabl"
		indentSize = 8
		expect = nil
	case s.Switches[useTabIndent]:
		indentChar = CharTabIndent
		indentSize = 1
		expect = []string{
			"	This is the",
			"	first",
			"	paragraph. It",
			"	has \\r\\n and",
			"	\\r words.",
			"\n",
			"\r\n",
			"	I will",
			"	however write",
			"	as long as I",
			"	want.",
			"	SuperblyGoodAndLongWishes!",
			"\r",
			"\r\n",
			"	This is the",
			"	first",
			"	paragraph. It",
			"	has \\r\\n and",
			"	\\r words.",
			"\n",
			"\r\n",
			"	I will",
			"	however write",
			"	as long as I",
			"	want.",
			"	SuperblyGoodAndLongWishes!",
			"\r",
			"\r\n",
		}
	default:
		indentChar = CharSpaceIndent
		indentSize = 8
		expect = []string{
			"        This is the",
			"        first",
			"        paragraph. It",
			"        has \\r\\n and",
			"        \\r words.",
			"\n",
			"\r\n",
			"        I will",
			"        however write",
			"        as long as I",
			"        want.",
			"        SuperblyGoodAndLongWishes!",
			"\r",
			"\r\n",
			"        This is the",
			"        first",
			"        paragraph. It",
			"        has \\r\\n and",
			"        \\r words.",
			"\n",
			"\r\n",
			"        I will",
			"        however write",
			"        as long as I",
			"        want.",
			"        SuperblyGoodAndLongWishes!",
			"\r",
			"\r\n",
		}
	}

	return sample, indentChar, indentSize, expect
}
