package strhelper

import (
	"testing"
)

func TestWordWrap(t *testing.T) {
	scenarios := testStrHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testWordWrap {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		styler := &Styler{}
		sample, limit, expect := s.prepareParagraph()

		// test
		out := styler.WordWrap(sample, limit)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStringSlices("subject", &out, "expect", &expect)
		s.log(th, map[string]interface{}{
			"input sample": sample,
			"expect":       expect,
			"input limit":  limit,
			"got":          out,
		})
		th.Conclude()
	}
}
