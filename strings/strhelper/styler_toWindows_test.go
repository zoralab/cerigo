package strhelper

import (
	"testing"
)

func TestToWindows(t *testing.T) {
	scenarios := testStrHelperScenarios()

	for i, s := range scenarios {
		if s.TestType != testToWindows {
			continue
		}

		// prepare
		th := s.prepareTHelper(t)
		styler := &Styler{}
		sample, expect := s.prepareString()

		// test
		out := styler.ToWindows(sample)

		// assert
		th.ExpectUIDCorrectness(i, s.UID, false)
		th.ExpectSameStrings("Subject", out, "Expectee", expect)
		s.log(th, map[string]interface{}{
			"sample": sample,
			"expect": expect,
			"got":    out,
		})
		th.Conclude()
	}
}
